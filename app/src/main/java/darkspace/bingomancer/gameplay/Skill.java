package darkspace.bingomancer.gameplay;

import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Random;

import darkspace.bingomancer.R;
import darkspace.bingomancer.activities.Game;
import darkspace.bingomancer.bingo.Card;
import darkspace.bingomancer.bingo.Cell;
import darkspace.bingomancer.data.Const;
import darkspace.bingomancer.data.GameResources;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.utils.GameMusic;

public class Skill {

    public enum Name {DEFENDER, FREE_TAP, GOLD_RAIN, HEAL, MANA_RAIN, PATIENCE, RAGE, SCAVENGE, WISDOM, UNKNOWN}

    private Player player;
    private Card card;
    private Game game;

    private final static int CHECK_CONTENT = 0;
    private final static int CHECK_DAUBED = 1;
    private final static int CHECK_COLOR = 2;

    public final static int DEFENDER_COST = 8;
    public final static int FREE_TAP_COST = 10;
    public final static int GOLD_RAIN_COST = 4;
    public final static int HEAL_COST = 6;
    public final static int MAGIC_CHANNEL_COST = 4;
    public final static int PATIENCE_COST = 8;
    public final static int RAGE_COST = 8;
    public final static int SCAVENGER_COST = 8;
    public final static int WISDOM_COST = 4;

    private final static int FREE_TAP_MOD = 10;
    private final static int GOLD_RAIN_MOD = 6;
    private final static int HEAL_MOD = 6;
    private final static int MAGIC_CHANNEL_MOD = 6;
    private final static int RAGE_MOD = 6;
    private final static int SCAVENGER_MOD = 10;
    private final static int WISDOM_MOD = 6;

    public Skill(Game game, Player player, Card card) {
        this.game = game;
        this.player = player;
        this.card = card;
    }

    public void use(int viewId) {
        boolean skillUsed = false;
        switch (viewId) {
            case R.id.useDefender:
                skillUsed = defender();
                break;
            case R.id.useFreeTap:
                skillUsed = freeTap();
                break;
            case R.id.useGoldRain:
                skillUsed = goldRain();
                break;
            case R.id.useHeal:
                skillUsed = heal();
                break;
            case R.id.useMagicChannel:
                skillUsed = magicChannel();
                break;
            case R.id.usePatience:
                skillUsed = patience();
                break;
            case R.id.useRage:
                skillUsed = rage();
                break;
            case R.id.useScavenger:
                skillUsed = scavenger();
                break;
            case R.id.useWisdom:
                skillUsed = wisdom();
                break;
        }
        if (skillUsed) {
            GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.SKILL_USED);
        } else {
            GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.LOW_MANA_OR_GOLD);
        }
    }

    private boolean wisdom() {
        if (player.getMagicPower()[0] < WISDOM_COST) {
            notEnoughMagicPower();
            return false;
        }
        int cellsToFill = player.getTotalMagic() / WISDOM_MOD + 1;
        if (fillCellWithContent(CHECK_CONTENT, Cell.Content.EXPERIENCE, false, cellsToFill) > 0) {
            payMagicPowerCost(WISDOM_COST);
            skillUsedMessage(Name.WISDOM);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.cellsFilled));
            return false;
        }
    }

    private boolean goldRain() {
        if (player.getMagicPower()[0] < GOLD_RAIN_COST) {
            notEnoughMagicPower();
            return false;
        }
        int cellsToFill = player.getTotalMagic() / GOLD_RAIN_MOD + 1;
        if (fillCellWithContent(CHECK_CONTENT, Cell.Content.GOLD, false, cellsToFill) > 0) {
            payMagicPowerCost(GOLD_RAIN_COST);
            skillUsedMessage(Name.GOLD_RAIN);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.cellsFilled));
            return false;
        }
    }

    private boolean scavenger() {
        if (player.getMagicPower()[0] < SCAVENGER_COST) {
            notEnoughMagicPower();
            return false;
        }
        int cellsToFill = player.getTotalMagic() / SCAVENGER_MOD + 1;
        if (fillCellWithContent(CHECK_CONTENT, Cell.Content.ITEM, false, cellsToFill) > 0) {
            payMagicPowerCost(SCAVENGER_COST);
            skillUsedMessage(Name.SCAVENGE);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.cellsFilled));
            return false;
        }
    }

    private boolean heal() {
        if (player.getMagicPower()[0] < HEAL_COST) {
            notEnoughMagicPower();
            return false;
        }
        int cellsToFill = player.getTotalMagic() / HEAL_MOD + 1;
        if (fillCellWithContent(CHECK_CONTENT, Cell.Content.HEAL, false, cellsToFill) > 0) {
            payMagicPowerCost(HEAL_COST);
            skillUsedMessage(Name.HEAL);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.cellsFilled));
            return false;
        }
    }

    private boolean rage() {
        if (player.getMagicPower()[0] < RAGE_COST) {
            notEnoughMagicPower();
            return false;
        }
        int cellsToFill = player.getTotalMagic() / RAGE_MOD + 1;
        if (fillCellWithContent(CHECK_CONTENT, Cell.Content.POWER, false, cellsToFill) > 0) {
            payMagicPowerCost(RAGE_COST);
            skillUsedMessage(Name.RAGE);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.cellsFilled));
            return false;
        }
    }

    private boolean freeTap() {
        if (player.getMagicPower()[0] < FREE_TAP_COST) {
            notEnoughMagicPower();
            return false;
        }
        ArrayList<Cell> numbers = createArrayWithCells(CHECK_DAUBED);
        Random rng = new Random();
        int cellsToDaub = player.getTotalMagic() / FREE_TAP_MOD + 1;
        int daubedCells = 0;
        while (cellsToDaub > 0 && numbers.size() > 0) {
            int index = rng.nextInt(numbers.size());
            Cell cell = numbers.get(index);
            game.daubCell(cell.getImageOfACell(), card, cell.getRow(), cell.getColumn(), cell.getTextViewOfACell());
            numbers.remove(index);
            cellsToDaub--;
            daubedCells++;
        }
        if (daubedCells > 0) {
            payMagicPowerCost(FREE_TAP_COST);
            skillUsedMessage(Name.FREE_TAP);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.cellsFilled));
            return false;
        }
    }

    private boolean magicChannel() {
        if (player.getMagicPower()[0] < MAGIC_CHANNEL_COST) {
            notEnoughMagicPower();
            return false;
        }
        int cellsToFill = player.getTotalMagic() / MAGIC_CHANNEL_MOD + 1;
        if (fillCellWithContent(CHECK_COLOR, Cell.Content.NOTHING, true, cellsToFill) > 0) {
            payMagicPowerCost(MAGIC_CHANNEL_COST);
            skillUsedMessage(Name.MANA_RAIN);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.cellsFilled));
            return false;
        }
    }

    private boolean patience() {
        if (player.getMagicPower()[0] < PATIENCE_COST) {
            notEnoughMagicPower();
            return false;
        }
        if (!player.isPatienceActive()) {
            player.setPatienceActive(true);
            skillUsedMessage(Name.PATIENCE);
            payMagicPowerCost(PATIENCE_COST);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.skillActive));
            return false;
        }
    }

    private boolean defender() {
        if (player.getMagicPower()[0] < DEFENDER_COST) {
            notEnoughMagicPower();
            return false;
        }
        if (!player.isDefenderActive()) {
            player.setDefenderActive(true);
            skillUsedMessage(Name.DEFENDER);
            payMagicPowerCost(DEFENDER_COST);
            return true;
        } else {
            game.updateMessages(game.getString(R.string.skillActive));
            return false;
        }
    }

    private void skillUsedMessage(Name skillName) {
        String name = "";
        switch (skillName) {
            case DEFENDER:
                name = game.getString(R.string.skillDefender);
                break;
            case FREE_TAP:
                name = game.getString(R.string.skillFreeTap);
                break;
            case GOLD_RAIN:
                name = game.getString(R.string.skillGoldRain);
                break;
            case HEAL:
                name = game.getString(R.string.skillHeal);
                break;
            case MANA_RAIN:
                name = game.getString(R.string.skillManaRain);
                break;
            case PATIENCE:
                name = game.getString(R.string.skillPatience);
                break;
            case RAGE:
                name = game.getString(R.string.skillRage);
                break;
            case SCAVENGE:
                name = game.getString(R.string.skillScavenge);
                break;
            case WISDOM:
                name = game.getString(R.string.skillWisdom);
                break;
        }
        String message = String.format(game.getString(R.string.skillUsed), name);
        game.updateMessages(message);
    }

    private void notEnoughMagicPower() {
        game.updateMessages(game.getString(R.string.lowMagicPower));
    }

    private void payMagicPowerCost(int cost) {
        int currentMagicPower = player.getMagicPower()[0];
        currentMagicPower -= cost;
        player.getMagicPower()[0] = currentMagicPower;
        game.getPlaMp().setText(String.valueOf(currentMagicPower));
    }

    private ArrayList<Cell> createArrayWithCells(int whatIsChecked) {
        ArrayList<Cell> array = new ArrayList<>();
        for (int i = 0; i < Const.CARD_SIDE; i++) {
            for (int j = 0; j < Const.CARD_SIDE; j++) {
                if (whatIsChecked == CHECK_COLOR) {
                    Cell.MagicColor color = card.getCells()[i][j].getColor();
                    if (color == Cell.MagicColor.NONE && !card.getCells()[i][j].isDaubed()) {
                        array.add(card.getCells()[i][j]);
                    }
                } else if (whatIsChecked == CHECK_CONTENT) {
                    Cell.Content content = card.getCells()[i][j].getContent();
                    if (content == Cell.Content.NOTHING && !card.getCells()[i][j].isDaubed()) {
                        array.add(card.getCells()[i][j]);
                    }
                } else {
                    if (!card.getCells()[i][j].isDaubed()) {
                        array.add(card.getCells()[i][j]);
                    }
                }
            }
        }
        return array;
    }

    private int fillCellWithContent(int whatIsBeingChecked, Cell.Content content, boolean fillNewColor, int cellsToFill) {
        Random rng = new Random();
        ArrayList<Cell> numbers = createArrayWithCells(whatIsBeingChecked);
        int filledCells = 0;
        while (cellsToFill > 0 && numbers.size() > 0) {
            int index = rng.nextInt(numbers.size());
            if (!fillNewColor) {
                numbers.get(index).setContent(content);
                numbers.get(index).getImageOfACell().setImageResource(GameResources.contentImage(content));
            } else {
                Cell.MagicColor colorToFill;
                int seed = rng.nextInt(99);
                if (seed < 33) {
                    colorToFill = Cell.MagicColor.RED;
                } else if (seed < 66) {
                    colorToFill = Cell.MagicColor.GREEN;
                } else {
                    colorToFill = Cell.MagicColor.BLUE;
                }
                numbers.get(index).setColor(colorToFill);
                ((RelativeLayout) numbers.get(index).getImageOfACell().getParent()).setBackgroundResource(GameResources.color(colorToFill));
            }
            numbers.remove(index);
            cellsToFill--;
            filledCells++;
        }
        return filledCells;
    }
}
