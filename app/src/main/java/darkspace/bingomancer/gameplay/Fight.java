package darkspace.bingomancer.gameplay;

import android.widget.TextView;

import java.util.Locale;
import java.util.Random;

import darkspace.bingomancer.R;
import darkspace.bingomancer.activities.Game;
import darkspace.bingomancer.actors.Monster;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.utils.GameMusic;

public class Fight {

    private static final int BASIC_HIT_CHANCE = 70;
    private static final float BLOCK_STRENGTH = 0.5f;
    private static final float DEFENDER_STRENGTH = 0.02f;
    private static final float DEFENDER_AGILITY_MOD = 0.8f;

    private Player player;
    private Monster monster;
    private Game game;

    private final static int ATTACK_MISSED = -99999999;

    public Fight(Game game, Player player, Monster monster) {
        this.game = game;
        this.player = player;
        this.monster = monster;
    }

    public void monsterIsAttacked() {
        if (player.isPoisoned()) {
            GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.MISS);
            game.updateMessages(game.getString(R.string.poisonedMessage));
            player.setPoisoned(false);
        } else {
            int damage = attack(
                    player.getTotalStrength(), player.getTotalAgility(), monster.getAgility(),
                    monster.getEndurance(), player.getWeapon().getDiceQuantity(), player.getWeapon().getDiceStrength(), 0);
            if (damage == ATTACK_MISSED) {
                damage = 0;
                GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.MISS);
                game.updateMessages(game.getString(R.string.playerMissed));
            } else {
                GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.HIT);
                game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.playerDealDamage), damage));
            }
            dealDamage(damage, monster.getHealth(), game.getMonHp());
        }
    }

    public void playerIsAttacked() {
        Random rng = new Random();
        int damage = attack(
                monster.getStrength(), monster.getAgility(), player.getTotalAgility(),
                player.getTotalEndurance(), monster.getDiceNumber(), monster.getDiceStrength(), player.getArmor().getDodge());
        if (damage != ATTACK_MISSED) {
            if (player.getShield().getBlock() >= rng.nextInt(100) + 1) {
                game.updateMessages(game.getString(R.string.blockActivated));
                GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.BLOCK);
                damage = Math.round(damage * BLOCK_STRENGTH);
            }
        }
        if (damage == ATTACK_MISSED) {
            damage = 0;
            game.updateMessages(game.getString(R.string.monsterMissed));
            GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.MONSTER_MISSED);
        } else if (player.isDefenderActive()) {
            damage = Math.round(damage * (1 - DEFENDER_STRENGTH * player.getTotalMagic()));
            if (damage < 0) {
                damage = 0;
            }
            String message = game.getString(R.string.defenderActivated) + String.format(Locale.getDefault(), game.getString(R.string.playerDamaged), damage);
            GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.BLOCK);
            game.updateMessages(message);
            player.setDefenderActive(false);
        } else {
            GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.MONSTER_ATTACK);
            game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.monsterAttacked), damage));
        }
        dealDamage(damage, player.getHealth(), game.getPlaHp());
    }

    public void missClick() {
        int damage = player.getLevel();
        dealDamage(damage, player.getHealth(), game.getPlaHp());
        String damageMessage = String.format(Locale.getDefault(), game.getString(R.string.playerDamaged), damage);
        GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.MISSCLICK);
        game.updateMessages(String.format(game.getString(R.string.numberNotDrawn), damageMessage));
    }

    private void dealDamage(int damage, int[] health, TextView textView) {
        health[0] -= damage;
        textView.setText(String.valueOf(health[0]));
    }

    private int attack(int attackerStrength, int attackerAgility, int defenderAgility, int defenderEndurance, int diceQuantity, int diceStrength, int defenderDodge) {
        int damage = 0;
        if (hitTest(attackerAgility, defenderAgility, defenderDodge)) {
            Random rng = new Random();
            for (int i = 0; i < diceQuantity; i++) {
                damage += rng.nextInt(diceStrength) + 1;
            }
            float mod = 1f;
            if (attackerStrength > defenderEndurance) {
                mod += 0.1f;
                mod += (attackerStrength - defenderEndurance) * 0.05f;
                if (mod > 2) {
                    mod = 2;
                }
            } else if (attackerStrength < defenderEndurance) {
                mod -= 0.1f;
                mod -= (defenderEndurance - attackerStrength) * 0.05f;
                if (mod < 0.5f) {
                    mod = 0.5f;
                }
            }
            damage = Math.round(damage * mod);
            if (damage <= 0) {
                damage = 1;
            }
        } else {
            damage = ATTACK_MISSED;
        }
        return damage;
    }

    private boolean hitTest(int attackerAgility, int defenderAgility, int dodge) {
        boolean attackHit = false;
        Random rng = new Random();
        if (dodge >= rng.nextInt(100) + 1) {
            return false;
        }
        int hitChance = BASIC_HIT_CHANCE;
        hitChance += attackerAgility;
        hitChance -= Math.round(defenderAgility * DEFENDER_AGILITY_MOD);

        if (hitChance > rng.nextInt(100) + 1) {
            attackHit = true;
        }
        return attackHit;
    }

    public int bingoDamage(int numberOfBingo) {
        float damage = player.getTotalStrength() * 0.8f;
        float baseMod = 1.1f;
        float secondMod = 1.15f;
        if (player.isPatienceActive() && numberOfBingo > 1) {
            baseMod = 1.15f;
            secondMod += player.getTotalMagic() * 0.005f;
            player.setPatienceActive(false);
        }
        for (int i = 0; i < numberOfBingo; i++) {
            baseMod *= secondMod;
        }
        damage *= baseMod;
        dealDamage((int) damage, monster.getHealth(), game.getMonHp());
        GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.BINGO);
        return (int) damage;
    }

    public void bingoMiss() {
        int damage = player.getLevel() + player.getMonsterLevel();
        dealDamage(damage, player.getHealth(), game.getPlaHp());
        GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.MISSCLICK);
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.noBingos), damage));
    }

    public boolean someoneIsDead() {
        return player.getHealth()[0] <= 0 || monster.getHealth()[0] <= 0;
    }

}
