package darkspace.bingomancer.gameplay;

import java.util.Locale;
import java.util.Random;

import darkspace.bingomancer.R;
import darkspace.bingomancer.activities.Game;
import darkspace.bingomancer.actors.Monster;
import darkspace.bingomancer.bingo.Cell;
import darkspace.bingomancer.data.Player;

public class ContentHandler {

    private Game game;
    private Player player;
    private Monster monster;

    private final static float MINIMAL_VALUE = 1;

    public ContentHandler(Game game, Player player, Monster monster) {
        this.game = game;
        this.player = player;
        this.monster = monster;
    }

    public void useContent(Cell.Content content) {
        switch (content) {
            case NOTHING:
                break;
            case GOLD:
                gold();
                break;
            case EXPERIENCE:
                experience();
                break;
            case ITEM:
                item();
                break;
            case POWER:
                power();
                break;
            case HEAL:
                heal();
                break;
            case CURSE:
                curse();
                break;
            case POISON:
                poison();
                break;
            case DAMAGE:
                damage();
                break;
        }
    }

    public void useColor(Cell.MagicColor color) {
        if (color == Cell.MagicColor.NONE) {
            return;
        }
        float magicMod = 0.006f;
        float magicPowerToHeal;
        if (color == player.getStrongColor()) {
            magicPowerToHeal = player.getTotalMagic() * magicMod * 1.5f;
        } else if (color == player.getWeakColor()) {
            magicPowerToHeal = player.getTotalMagic() * magicMod * 0.6f;
        } else {
            magicPowerToHeal = player.getTotalMagic() * magicMod * 1f;
        }
        magicPowerToHeal = magicPowerToHeal * player.getMagicPower()[1] * 0.4f;
        if (magicPowerToHeal < MINIMAL_VALUE) {
            magicPowerToHeal = MINIMAL_VALUE;
        }
        int newMagicPower = player.getMagicPower()[0] + Math.round(magicPowerToHeal);
        if (newMagicPower > player.getMagicPower()[1]) {
            int difference = newMagicPower - player.getMagicPower()[1];
            magicPowerToHeal -= difference;
            newMagicPower = player.getMagicPower()[1];
        }
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.mpReplenish), Math.round(magicPowerToHeal)));
        player.getMagicPower()[0] = newMagicPower;
        game.getPlaMp().setText(String.valueOf(newMagicPower));
    }

    private void gold() {
        Random rng = new Random();
        float goldBonus = 0;
        float maxGold = player.getTotalMagic() * 0.18f;
        for (int i = 0; i < player.getLevel(); i++) {
            goldBonus += rng.nextInt(Math.round(maxGold)) + 1;
        }
        if (goldBonus < MINIMAL_VALUE) {
            goldBonus = MINIMAL_VALUE;
        }
        int gold = player.getGold();
        player.setGold(gold + Math.round(goldBonus));
        game.getGold().setText(String.valueOf(player.getGold()));
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.goldBonus), Math.round(goldBonus)));
    }

    private void item() {
        int items = monster.getItemReward();
        items++;
        Random rng = new Random();
        if (player.getTotalMagic() >= rng.nextInt(100) + 1) {
            items++;
        }
        monster.setItemReward(items);
        game.updateMessages(game.getString(R.string.pickedChest));
    }

    private void power() {
        int monsterHealth = monster.getHealth()[0];
        float damage = (player.getTotalStrength() + player.getTotalMagic()) * 0.15f;
        if (damage < MINIMAL_VALUE) {
            damage = MINIMAL_VALUE;
        }
        monster.getHealth()[0] = monsterHealth - Math.round(damage);
        game.getMonHp().setText(String.valueOf(monsterHealth - damage));
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.playerDealDamage), Math.round(damage)));
    }

    private void heal() {
        float healValue = player.getHealth()[1] * player.getTotalMagic() * 0.005f;
        if (healValue < MINIMAL_VALUE) {
            healValue = MINIMAL_VALUE;
        }
        int newHealth = player.getHealth()[0] + Math.round(healValue);
        if (newHealth > player.getHealth()[1]) {
            int difference = newHealth - player.getHealth()[1];
            healValue -= difference;
            newHealth = player.getHealth()[1];
        }
        player.getHealth()[0] = newHealth;
        game.getPlaHp().setText(String.valueOf(newHealth));
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.playerHeal), Math.round(healValue)));
    }

    private void curse() {
        float damage = monster.getMagic() * 0.008f * player.getMagicPower()[1];
        if (damage < MINIMAL_VALUE) {
            damage = MINIMAL_VALUE;
        }
        int playerNewMp = player.getMagicPower()[0] - Math.round(damage);
        if (playerNewMp < 0) {
            damage += playerNewMp;
            playerNewMp = 0;
        }
        player.getMagicPower()[0] = playerNewMp;
        game.getPlaMp().setText(String.valueOf(playerNewMp));
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.mpDecrease), Math.round(damage)));
    }

    private void poison() {
        player.setPoisoned(true);
        game.updateMessages(game.getString(R.string.poisoned));
    }

    private void damage() {
        int damage = 0;
        damage += monster.getMagic();
        if (damage < MINIMAL_VALUE) {
            damage = (int) MINIMAL_VALUE;
        }
        int playerNewHp = player.getHealth()[0] - damage;
        if (playerNewHp < 1) {
            damage += playerNewHp;
            playerNewHp = 1;
        }
        player.getHealth()[0] = playerNewHp;
        game.getPlaHp().setText(String.valueOf(playerNewHp));
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.playerDamaged), damage));
    }

    private void experience() {
        float experienceBonus = player.getTotalMagic() * 0.75f * player.getLevel() * 0.1f;
        if (experienceBonus < MINIMAL_VALUE) {
            experienceBonus = MINIMAL_VALUE;
        }
        int newExperienceValue = player.getExperience()[0] + Math.round(experienceBonus);
        player.getExperience()[0] = newExperienceValue;
        game.getExperience().setText(String.valueOf(newExperienceValue));
        game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.experienceBonus), Math.round(experienceBonus)));
    }
}
