package darkspace.bingomancer.actors;

import android.content.Context;

import java.util.Random;

import darkspace.bingomancer.R;

public class Monster {

    public enum Type {STRONG, RESISTANT, AGILE, MAGICAL, CURSED, POISONOUS, DAMAGE, BOSS}

    private int strength;
    private int magic;
    private int agility;
    private int endurance;
    private int attackCounter;
    private long interval;

    private int diceNumber;
    private int diceStrength;

    private int[] health;
    private int level;

    private int goldReward;
    private int experienceReward;
    private int itemReward;

    private int curseTiles;
    private int damageTiles;
    private int poisonTiles;
    private Type type;
    private String name;

    public Monster(int level, Context context) {
        createMonster(level, context);
    }

    public void createMonster(int level, Context context) {
        this.level = level;
        type = checkType();
        name = selectName(context);

        strength = setStat(Type.STRONG);
        magic = setStat(Type.MAGICAL);
        agility = setStat(Type.AGILE);
        endurance = setStat(Type.RESISTANT);

        attackCounter = attackCounter();
        interval = interval();

        diceNumber = 1 + level / 4;
        diceStrength = 1 + level / 3;

        boolean[] tiles = {false, false, false};
        if (type == Type.MAGICAL) {
            Random rng = new Random();
            tiles[rng.nextInt(tiles.length)] = true;
        }
        poisonTiles = createTiles(Type.POISONOUS, tiles[0]);
        curseTiles = createTiles(Type.CURSED, tiles[1]);
        damageTiles = createTiles(Type.DAMAGE, tiles[2]);

        int maxHealth = maximumHealth();
        health = new int[]{maxHealth, maxHealth};

        experienceReward = experienceReward();
        goldReward = goldReward();
        itemReward = 1;
        if (type == Type.BOSS) {
            itemReward++;
        }
    }

    private Type checkType() {
        if (level % 5 == 0) {
            return Type.BOSS;
        } else {
            Type[] types = Type.values();
            Random rng = new Random();
            return types[rng.nextInt(types.length - 1)];
        }
    }

    public long checkInterval(int playerAgility) {
        long calculatedInterval = interval + (playerAgility - agility) * 75;
        if (calculatedInterval < 1000) {
            calculatedInterval = 1000;
        }
        return calculatedInterval;
    }

    private int setStat(Type typeOfStat) {
        Random rng = new Random();
        int value = 0;
        int mod = 3;
        if (type == typeOfStat || type == Type.BOSS) {
            mod = 5;
        }
        for (int i = 0; i < level; i++) {
            value += rng.nextInt(mod);
        }
        value += level;
        return value;
    }

    private int maximumHealth() {
        int mod = 4;
        if (type == Type.RESISTANT || type == Type.BOSS) {
            mod = 6;
        }
        return strength + endurance * 4 + level * mod;
    }

    private long interval() {
        int value = 7000;
        int mod = strength + magic + endurance;
        if (type == Type.AGILE || type == Type.BOSS) {
            mod += agility * 3;
        } else {
            mod += agility * 2;
        }
        value -= mod * 15;
        if (value < 2000) {
            value = 2000;
        }
        return value;
    }

    private int attackCounter() {
        int value = 7;
        int mod = (strength + agility + endurance + magic) / 20;
        if (value - mod < 2) {
            return 2;
        } else {
            return value - mod;
        }
    }

    private int experienceReward() {
        int value = (strength + endurance + agility + magic + level) * 2;
        if (type == Type.BOSS) {
            value *= 2;
        }
        value *= 2;
        return value;
    }

    private int goldReward() {
        int value = health[1] / 10;
        if (type == Type.BOSS) {
            value *= 2;
        }
        return value;
    }

    private int calculateTilesNumber(int base, int max, int divider) {
        int tiles = base + level / divider;
        if (tiles > max) {
            tiles = max;
        }
        return tiles;
    }

    private int createTiles(Type monsterType, boolean tileForMagicalMonster) {
        int tiles = 0;
        if (type == monsterType) {
            return calculateTilesNumber(3, 6, 5);
        } else if (type == Type.BOSS) {
            return calculateTilesNumber(1, 3, 6);
        } else if (type == Type.MAGICAL && tileForMagicalMonster) {
            return calculateTilesNumber(1, 3, 6);
        } else {
            return tiles;
        }
    }

    private String selectName(Context context) {
        switch (type) {
            case STRONG:
                return context.getString(R.string.monsterNameStrong);
            case RESISTANT:
                return context.getString(R.string.monsterNameResistant);
            case AGILE:
                return context.getString(R.string.monsterNameAgile);
            case MAGICAL:
                return context.getString(R.string.monsterNameMagic);
            case CURSED:
                return context.getString(R.string.monsterNameCursed);
            case POISONOUS:
                return context.getString(R.string.monsterNamePoisonous);
            case DAMAGE:
                return context.getString(R.string.monsterNameSpiky);
            case BOSS:
                return context.getString(R.string.monsterNameBoss);
            default:
                return "";
        }
    }

    public int getStrength() {
        return strength;
    }

    public int getMagic() {
        return magic;
    }

    public int getAgility() {
        return agility;
    }

    public int getEndurance() {
        return endurance;
    }

    public int[] getHealth() {
        return health;
    }

    public int getAttackCounter() {
        return attackCounter;
    }

    public int getGoldReward() {
        return goldReward;
    }

    public int getExperienceReward() {
        return experienceReward;
    }

    public int getCurseTiles() {
        return curseTiles;
    }

    public int getDamageTiles() {
        return damageTiles;
    }

    public int getPoisonTiles() {
        return poisonTiles;
    }

    public Type getType() {
        return type;
    }

    public int getItemReward() {
        return itemReward;
    }

    public void setItemReward(int itemReward) {
        this.itemReward = itemReward;
    }

    public int getDiceNumber() {
        return diceNumber;
    }

    public String getName() {
        return name;
    }

    public int getDiceStrength() {
        return diceStrength;
    }
}
