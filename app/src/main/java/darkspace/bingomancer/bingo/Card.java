package darkspace.bingomancer.bingo;

import java.util.ArrayList;
import java.util.Random;

import darkspace.bingomancer.data.Const;

public class Card {

    private Cell[][] cells;
    private ArrayList<Integer> numbersOnACard;
    private ArrayList<int[][]> patterns;
    private int healCells;
    private int powerCells;
    private int itemCells;
    private int experienceCells;
    private int goldCells;

    public Card(int monsterLevel, int curse, int damage, int poison) {
        healCells = 1;
        powerCells = 1;
        itemCells = 1;
        experienceCells = 2;
        goldCells = 2;
        patterns = new ArrayList<>();
        numbersOnACard = new ArrayList<>();

        createNewCard(monsterLevel, curse, damage, poison);
    }

    private void assignNumbers() {
        numbersOnACard.clear();
        int i = 0;
        int min;
        int max;
        Random rng = new Random();
        cells = new Cell[Const.CARD_SIDE][Const.CARD_SIDE];
        for (int j = 0; j < Const.CARD_SIDE; j++) {
            for (int k = 0; k < Const.CARD_SIDE; k++) {
                cells[j][k] = new Cell();
                max = 75 - Math.abs(k - 4) * Const.NUMBER_GROUP;
                min = max - 14;
                while (checkIfNumberExists(i, numbersOnACard) || i == 0) {
                    i = rng.nextInt(Const.NUMBER_GROUP) + min;
                }
                cells[j][k].setNumber(i);
                cells[j][k].setRow(j);
                cells[j][k].setColumn(k);

                numbersOnACard.add(i);
            }
        }
    }

    private boolean checkIfNumberExists(int number, ArrayList<Integer> drawnNumbers) {
        int i = drawnNumbers.size();
        boolean numberExist = false;
        for (int j = 0; j < i; j++) {
            if (drawnNumbers.get(j) == number) {
                numberExist = true;
                break;
            }
        }
        return numberExist;
    }

    private void setColors(int red, int green, int blue) {
        while (red > 0 || green > 0 || blue > 0) {
            if (red > 0) {
                red -= checkColor(Cell.MagicColor.RED);
            }
            if (green > 0) {
                green -= checkColor(Cell.MagicColor.GREEN);
            }
            if (blue > 0) {
                blue -= checkColor(Cell.MagicColor.BLUE);
            }
        }
    }

    private int checkColor(Cell.MagicColor color) {
        Random rng = new Random();
        int x = rng.nextInt(Const.CARD_SIDE);
        int y = rng.nextInt(Const.CARD_SIDE);
        if (cells[x][y].getColor() == Cell.MagicColor.NONE) {
            cells[x][y].setColor(color);
            return 1;
        } else {
            return 0;
        }
    }

    private void setContent(int gold, int experience, int item, int power, int heal, int poison, int curse, int damage) {
        while (gold > 0 || experience > 0 || item > 0 || power > 0 || heal > 0) {
            if (gold > 0) {
                gold -= checkContent(Cell.Content.GOLD);
            }
            if (experience > 0) {
                experience -= checkContent(Cell.Content.EXPERIENCE);
            }
            if (item > 0) {
                item -= checkContent(Cell.Content.ITEM);
            }
            if (power > 0) {
                power -= checkContent(Cell.Content.POWER);
            }
            if (heal > 0) {
                heal -= checkContent(Cell.Content.HEAL);
            }
            if (poison > 0) {
                poison -= checkContent(Cell.Content.POISON);
            }
            if (curse > 0) {
                curse -= checkContent(Cell.Content.CURSE);
            }
            if (damage > 0) {
                damage -= checkContent(Cell.Content.DAMAGE);
            }
        }
    }

    private int checkContent(Cell.Content content) {
        Random rng = new Random();
        int x = rng.nextInt(Const.CARD_SIDE);
        int y = rng.nextInt(Const.CARD_SIDE);
        if (cells[x][y].getContent() == Cell.Content.NOTHING) {
            cells[x][y].setContent(content);
            return 1;
        } else {
            return 0;
        }
    }

    public int checkForBingo() {
        int bingos = 0;
        for (int i = 0; i < patterns.size(); i++) {
            boolean continueCheck = true;
            while (continueCheck) {
                int x;
                int y;
                for (int j = 0; j < patterns.get(i).length; j++) {
                    x = patterns.get(i)[j][0];
                    y = patterns.get(i)[j][1];
                    if (!cells[x][y].isDaubed()) {
                        continueCheck = false;
                        break;
                    }
                }
                if (continueCheck) {
                    continueCheck = false;
                    bingos++;
                }
            }
        }
        return bingos;
    }

    private void createPatternsArray() {
        patterns.clear();
        int[][] pattern_01 = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}};
        int[][] pattern_02 = {{1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4}};
        int[][] pattern_03 = {{2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4}};
        int[][] pattern_04 = {{3, 0}, {3, 1}, {3, 2}, {3, 3}, {3, 4}};
        int[][] pattern_05 = {{4, 0}, {4, 1}, {4, 2}, {4, 3}, {4, 4}};

        int[][] pattern_06 = {{0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}};
        int[][] pattern_07 = {{0, 1}, {1, 1}, {2, 1}, {3, 1}, {4, 1}};
        int[][] pattern_08 = {{0, 2}, {1, 2}, {2, 2}, {3, 2}, {4, 2}};
        int[][] pattern_09 = {{0, 3}, {1, 3}, {2, 3}, {3, 3}, {4, 3}};
        int[][] pattern_10 = {{0, 4}, {1, 4}, {2, 4}, {3, 4}, {4, 4}};

        int[][] pattern_11 = {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}};
        int[][] pattern_12 = {{0, 4}, {1, 3}, {2, 2}, {3, 1}, {4, 0}};

        int[][] pattern_13 = {{0, 0}, {0, 4}, {2, 2}, {4, 0}, {4, 4}};

        patterns.add(pattern_01);
        patterns.add(pattern_02);
        patterns.add(pattern_03);
        patterns.add(pattern_04);
        patterns.add(pattern_05);
        patterns.add(pattern_06);
        patterns.add(pattern_07);
        patterns.add(pattern_08);
        patterns.add(pattern_09);
        patterns.add(pattern_10);
        patterns.add(pattern_11);
        patterns.add(pattern_12);
        patterns.add(pattern_13);
    }

    public void createNewCard(int monsterLevel, int curse, int damage, int poison) {
        createPatternsArray();
        assignNumbers();
        int colors = 2 + monsterLevel / 4;
        if (colors > 5) {
            colors = 5;
        }
        setColors(colors, colors, colors);
        setContent(goldCells, experienceCells, itemCells, powerCells, healCells, poison, curse, damage);
    }

    public Cell[][] getCells() {
        return cells;
    }

    protected ArrayList<Integer> getNumbersOnACard() {
        return numbersOnACard;
    }
}