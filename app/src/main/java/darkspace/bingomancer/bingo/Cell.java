package darkspace.bingomancer.bingo;

import android.widget.ImageView;
import android.widget.TextView;

public class Cell {

    public enum Content {NOTHING, GOLD, EXPERIENCE, ITEM, POWER, HEAL, CURSE, POISON, DAMAGE}

    public enum MagicColor {BLUE, RED, GREEN, NONE}

    private int number;
    private boolean daubed;
    private MagicColor color;
    private Content content;
    private int row;
    private int column;
    private ImageView imageOfACell;
    private TextView textViewOfACell;

    protected Cell() {
        number = 0;
        color = MagicColor.NONE;
        daubed = false;
        content = Content.NOTHING;
        row = 0;
        column = 0;
    }

    public int getNumber() {
        return number;
    }

    protected void setNumber(int number) {
        this.number = number;
    }

    public boolean isDaubed() {
        return daubed;
    }

    public void setDaubed(boolean daubed) {
        this.daubed = daubed;
    }

    public MagicColor getColor() {
        return color;
    }

    public void setColor(MagicColor color) {
        this.color = color;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public int getRow() {
        return row;
    }

    protected void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    protected void setColumn(int column) {
        this.column = column;
    }

    public ImageView getImageOfACell() {
        return imageOfACell;
    }

    public void setImageOfACell(ImageView imageOfACell) {
        this.imageOfACell = imageOfACell;
    }

    public TextView getTextViewOfACell() {
        return textViewOfACell;
    }

    public void setTextViewOfACell(TextView textViewOfACell) {
        this.textViewOfACell = textViewOfACell;
    }
}
