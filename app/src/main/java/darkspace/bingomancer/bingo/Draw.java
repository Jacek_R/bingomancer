package darkspace.bingomancer.bingo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import darkspace.bingomancer.data.Player;

public class Draw {
    private ArrayList<Boolean> drawnNumbers;
    private ArrayList<Integer> remainingNumbers;

    private Player player;
    private Card card;

    private static final int CHANCE_FOR_REPEATING_A_DRAW = 20;

    public Draw(Player player, Card card) {
        this.player = player;
        this.card = card;
        drawnNumbers = new ArrayList<>();
        remainingNumbers = new ArrayList<>();
        createNewDraw();
    }

    public int drawNumber() {
        if (remainingNumbers.size() == 0) {
            return 0;
        } else {
            Random rng = new Random();
            int index = rng.nextInt(remainingNumbers.size());
            int number = remainingNumbers.get(index);
            int luckValue = player.getTotalLuck();
            boolean numberIsOnACard = checkIfNumberExistsOnACard(number);
            while (!numberIsOnACard && luckValue > 0) {
                boolean luckTestPassed = luckTest(luckValue);
                if (luckTestPassed) {
                    index = rng.nextInt(remainingNumbers.size());
                    number = remainingNumbers.get(index);
                    numberIsOnACard = checkIfNumberExistsOnACard(number);
                }
                luckValue -= CHANCE_FOR_REPEATING_A_DRAW;
            }
            drawnNumbers.set(number, true);
            remainingNumbers.remove(index);
            return number;
        }
    }

    private boolean luckTest(int luck) {
        boolean testPassed = false;
        Random rng = new Random();
        if (luck >= rng.nextInt(CHANCE_FOR_REPEATING_A_DRAW) + 1) {
            testPassed = true;
        }
        return testPassed;
    }

    private boolean checkIfNumberExistsOnACard(int drawnNumber) {
        boolean numberExists = false;
        for (int number : card.getNumbersOnACard()) {
            if (number == drawnNumber) {
                numberExists = true;
                break;
            }
        }
        return numberExists;
    }

    private void createRemainingNumbers() {
        remainingNumbers.clear();
        for (int i = 0; i < 75; i++) {
            remainingNumbers.add(i + 1);
        }
        Collections.shuffle(remainingNumbers);
    }

    private void createInitialNumbers() {
        drawnNumbers.clear();
        for (int i = 0; i < 76; i++) {
            drawnNumbers.add(false);
        }
    }

    public void createNewDraw() {
        createInitialNumbers();
        createRemainingNumbers();
    }

    public ArrayList<Boolean> getDrawnNumbers() {
        return drawnNumbers;
    }

    public ArrayList<Integer> getRemainingNumbers() {
        return remainingNumbers;
    }
}
