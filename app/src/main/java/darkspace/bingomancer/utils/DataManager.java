package darkspace.bingomancer.utils;

import com.google.gson.Gson;

import darkspace.bingomancer.actors.Monster;
import darkspace.bingomancer.data.Player;

public class DataManager {

    public static String createJson(Object object) {
        return new Gson().toJson(object);
    }

    public static Player createPlayerFromJson(String json) {
        return new Gson().fromJson(json, Player.class);
    }

    public static Monster createMonsterFromJson(String json) {
        return new Gson().fromJson(json, Monster.class);
    }
}
