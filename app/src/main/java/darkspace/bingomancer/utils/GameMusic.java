package darkspace.bingomancer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;

import darkspace.bingomancer.R;
import im.delight.android.audio.MusicManager;

public class GameMusic {

    private static final float VOLUME = 0.15f;
    public static final String MUSIC_PREFERENCES = "PreferencesKeyForMusicValues";

    private static GameMusic ourInstance;
    private MediaPlayer music;

    public final static int MENU = R.raw.bgm_menu;
    public final static int GAME = R.raw.bgm_game;

    public final static int TAP = R.raw.tap;
    public final static int HIT = R.raw.hit;
    public final static int DRAW = R.raw.draw;
    public final static int SELL = R.raw.sell;
    public final static int MISS = R.raw.miss;
    public final static int BOMB = R.raw.bomb;
    public final static int DEATH = R.raw.death;
    public final static int BLOCK = R.raw.block;
    public final static int BINGO = R.raw.bingo;
    public final static int POTION = R.raw.potion;
    public final static int FIGHT_WON = R.raw.won;
    public final static int LEVEL_UP = R.raw.level_up;
    public final static int MISSCLICK = R.raw.missclick;
    public final static int SKILL_USED = R.raw.skill_used;
    public final static int LOW_MANA_OR_GOLD = R.raw.low_mana;
    public final static int MONSTER_ATTACK = R.raw.monster_attack;
    public final static int MONSTER_MISSED = R.raw.monster_missed;

    public static boolean musicOn;
    public static boolean soundsOn;

    public static int activities_open = 0;

    public static final String MUSIC_KEY = "musicIsOffOrOn";
    public static final String SOUND_KEY = "soundsAreOffOrOn";

    public static GameMusic getInstance() {
        if (ourInstance == null) {
            ourInstance = new GameMusic();
        }
        return ourInstance;
    }

    private GameMusic() {
    }

    public void loadTrack(Context context, int track) {
        if (music != null) {
            if (music.isPlaying()) {
                music.release();
            }
        }
        if (musicOn) {
            music = MediaPlayer.create(context, track);
            music.setVolume(VOLUME, VOLUME);
            music.setLooping(true);
            switchPlayback();
        }
    }

    public void destroyMusic() {
        if (activities_open == 0) {
            if (music != null) {
                music.stop();
                music.release();
                music = null;
            }
        }
    }

    public void playSound(Context context, int id) {
        if (soundsOn) {
            MusicManager.getInstance().play(context, id);
        }
    }

    private void switchPlayback() {
        if (music != null) {
            if (music.isPlaying()) {
                music.pause();
            } else {
                music.start();
            }
        }
    }

    public void stopMusic() {
        if (music != null) {
            music.stop();
        }
    }

    public void pauseMusic() {
        if (music != null) {
            music.pause();
        }
    }

    public void continuePlaying() {
        if (music != null) {
            music.start();
        }
    }

    public void initiateMusicPreferences(Context context) {
        SharedPreferences pref = context.getSharedPreferences(MUSIC_PREFERENCES, Context.MODE_PRIVATE);
        if (pref.contains(GameMusic.MUSIC_KEY)) {
            GameMusic.musicOn = pref.getBoolean(GameMusic.MUSIC_KEY, true);
            GameMusic.soundsOn = pref.getBoolean(GameMusic.SOUND_KEY, true);
        } else {
            soundsOn = true;
            musicOn = true;
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(GameMusic.MUSIC_KEY, musicOn);
            editor.putBoolean(GameMusic.SOUND_KEY, soundsOn);
            editor.apply();
        }
    }

    public MediaPlayer getMusic() {
        return music;
    }
}
