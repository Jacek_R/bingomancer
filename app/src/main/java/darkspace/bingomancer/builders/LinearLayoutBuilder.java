package darkspace.bingomancer.builders;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public abstract class LinearLayoutBuilder {

    public static LinearLayout createLinearLayout(Context context, ViewGroup.LayoutParams params, float weightSum, int orientation) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(params);
        linearLayout.setWeightSum(weightSum);
        linearLayout.setOrientation(orientation);
        return linearLayout;
    }

    public static LinearLayout createLinearLayout(Context context, ViewGroup.LayoutParams params, int orientation) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(orientation);
        return linearLayout;
    }

}
