package darkspace.bingomancer.builders;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lb.auto_fit_textview.AutoResizeTextView;

import uk.co.chrisjenx.calligraphy.CalligraphyUtils;

public abstract class TextViewBuilder {

    private final static String FONT = "fonts/casual.ttf";

    public static TextView createText(
            Context context, String text, ViewGroup.LayoutParams params, int gravity, int textColor, int style) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setLayoutParams(params);
        textView.setGravity(gravity);
        textView.setTextColor(textColor);
        textView.setTypeface(null, style);
        CalligraphyUtils.applyFontToTextView(context, textView, FONT);
        return textView;
    }

    protected static AutoResizeTextView createAutoText(Context context) {
        return new AutoResizeTextView(context);
    }

    protected static AutoResizeTextView createAutoText(Context context, String message, ViewGroup.LayoutParams params, int gravity) {
        AutoResizeTextView textView = new AutoResizeTextView(context);
        textView.setText(message);
        textView.setLayoutParams(params);
        textView.setGravity(gravity);
        textView.setFocusable(false);
        textView.setClickable(false);
        CalligraphyUtils.applyFontToTextView(context, textView, FONT);
        return textView;
    }
}
