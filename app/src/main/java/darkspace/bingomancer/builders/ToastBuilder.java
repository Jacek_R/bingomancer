package darkspace.bingomancer.builders;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import darkspace.bingomancer.R;

public abstract class ToastBuilder {

    private static Toast lastToast;

    public static Toast create(String message, Context context) {
        if (lastToast != null) {
            lastToast.cancel();
        }
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.custom_toast, ((ViewGroup) ((Activity) context).findViewById(R.id.frameRoot)));

        TextView text = (TextView) layout.findViewById(R.id.customToastText);
        text.setText(message);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        lastToast = toast;
        return toast;
    }

}
