package darkspace.bingomancer.builders;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import darkspace.bingomancer.R;

public class GameOverDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_over_dialog, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        //noinspection ConstantConditions
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button quitGame = (Button) view.findViewById(R.id.buttonQuit);
        Button notQuitGame = (Button) view.findViewById(R.id.buttonNotQuit);

        quitGame.setOnClickListener(clickQuit());
        notQuitGame.setOnClickListener(clickNotQuit());
        return view;
    }

    private View.OnClickListener clickQuit() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finishAffinity();
            }
        };
    }

    private View.OnClickListener clickNotQuit() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
            }
        };
    }
}