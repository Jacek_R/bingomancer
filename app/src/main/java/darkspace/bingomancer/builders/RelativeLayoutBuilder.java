package darkspace.bingomancer.builders;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public abstract class RelativeLayoutBuilder {

    public static RelativeLayout createRelativeLayout(Context context, ViewGroup.LayoutParams params, int borderColorResource) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(params);
        relativeLayout.setBackgroundResource(borderColorResource);
        return relativeLayout;
    }
}
