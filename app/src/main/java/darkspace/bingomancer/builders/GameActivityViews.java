package darkspace.bingomancer.builders;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.lb.auto_fit_textview.AutoResizeTextView;

import java.util.ArrayList;

import darkspace.bingomancer.R;
import darkspace.bingomancer.data.Const;

public abstract class GameActivityViews {

    public static PopupWindow createPopUp(Context context, Handler handler, Button button, int margin, LinearLayout grid, boolean monsterDied) {
        handler.removeCallbacksAndMessages(null);

        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewParams.setMargins(margin, margin, margin, margin);
        LinearLayout mainWindow = new LinearLayout(context);

        final PopupWindow window = new PopupWindow(mainWindow, ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        mainWindow.setLayoutParams(viewParams);
        mainWindow.setOrientation(LinearLayout.VERTICAL);
        mainWindow.setGravity(Gravity.CENTER);

        TextView text = TextViewBuilder.createText(context, "", viewParams, Gravity.CENTER, Color.BLACK, Typeface.BOLD_ITALIC);

        if (monsterDied) {
            text.setText(R.string.monsterDied);
        } else {
            text.setText(R.string.died);
        }
        button.setBackgroundResource(R.drawable.selector_btn_medium_bg);
        button.setGravity(Gravity.CENTER);

        mainWindow.addView(text);
        mainWindow.addView(button);
        mainWindow.setBackgroundResource(R.drawable.gui_rectangle_window);
        mainWindow.setPadding(margin, margin, margin, margin);

        window.setFocusable(true);
        window.setContentView(mainWindow);
        window.setAnimationStyle(R.style.AnimationPopup);
        window.showAtLocation(grid, Gravity.CENTER, 0, 0);

        return window;
    }

    public static void createDrawnNumbers(
            Context context, LinearLayout drawnNumbers, ArrayList<AutoResizeTextView> numbersTextViews, int margin) {
        drawnNumbers.removeAllViews();
        numbersTextViews.clear();
        numbersTextViews.add(TextViewBuilder.createAutoText(context));
        int row = 0;
        int column = 0;
        int counter = 1;
        while (column < Const.CARD_SIDE) {
            int weight = 1;
            float weightSum = Const.NUMBER_GROUP;
            LinearLayout.LayoutParams parentParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, weight);
            parentParams.setMargins(margin, margin, margin, margin);
            LinearLayout parent = LinearLayoutBuilder.createLinearLayout(context, parentParams, weightSum, LinearLayout.VERTICAL);

            while (row < Const.NUMBER_GROUP) {
                LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, 0, weight);

                AutoResizeTextView textView = TextViewBuilder.createAutoText(context, String.valueOf(counter), textParams, Gravity.CENTER);
                textView.setTextColor(Color.BLACK);
                numbersTextViews.add(textView);

                parent.addView(textView);
                counter++;
                row++;
            }
            drawnNumbers.addView(parent);
            row = 0;
            column++;
        }
    }

}
