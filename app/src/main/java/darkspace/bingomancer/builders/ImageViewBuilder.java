package darkspace.bingomancer.builders;

import android.content.Context;
import android.widget.ImageView;

public abstract class ImageViewBuilder {

    public static ImageView createImage(Context context, int imageResource) {
        ImageView image = new ImageView(context);
        if (imageResource != 0) {
            image.setImageResource(imageResource);
        }
        return image;
    }

}
