package darkspace.bingomancer.items;

public class Shield extends Item {

    public Shield() {
        strength = 0;
        endurance = 0;
        agility = 0;
        magic = 0;
        luck = 0;
        quality = Quality.NORMAL;
        block = 0;
    }

    public Shield(int playerLuck, int monsterLevel) {
        super(playerLuck, monsterLevel);
        statsToChoose.add(TypesOfStats.ENDURANCE);
        statsToChoose.add(TypesOfStats.STRENGTH);
        statsToChoose.add(TypesOfStats.MAGIC);
        determineStats(playerLuck, monsterLevel);
        block = (monsterLevel / (4 - qualityToStatistics())) / 2;
        block++;
    }

    public void updateItem(int strength, int endurance, int agility, int magic, int luck, int block, Quality quality) {
        super.updateItem(strength, endurance, agility, magic, luck, quality);
        this.block = block;
    }
}
