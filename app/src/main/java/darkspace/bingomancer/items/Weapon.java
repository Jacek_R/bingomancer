package darkspace.bingomancer.items;

public class Weapon extends Item {

    public Weapon() {
        strength = 0;
        endurance = 0;
        agility = 0;
        magic = 0;
        luck = 0;
        quality = Quality.NORMAL;
        diceQuantity = 2;
        diceStrength = 2;
    }

    public Weapon(int playerLuck, int monsterLevel) {
        super(playerLuck, monsterLevel);
        statsToChoose.add(TypesOfStats.STRENGTH);
        statsToChoose.add(TypesOfStats.MAGIC);
        statsToChoose.add(TypesOfStats.AGILITY);
        statsToChoose.add(TypesOfStats.LUCK);
        determineStats(playerLuck, monsterLevel);
        diceQuantity = 1 + qualityToStatistics();
        diceStrength = determineDiceStrength(monsterLevel);
    }

    private int determineDiceStrength(int monsterLevel) {
        float strength = monsterLevel / 2 + (qualityToStatistics() * (1 + monsterLevel * 0.075f));
        strength /= 2f;
        return Math.round(strength);
    }

    public void updateItem(int strength, int endurance, int agility, int magic, int luck, int diceStrength, int diceQuantity, Quality quality) {
        super.updateItem(strength, endurance, agility, magic, luck, quality);
        this.diceStrength = diceStrength;
        this.diceQuantity = diceQuantity;
    }
}
