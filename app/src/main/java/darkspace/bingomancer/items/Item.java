package darkspace.bingomancer.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Item implements Serializable {

    public enum Quality {NORMAL, MAGIC, LEGENDARY}

    protected enum TypesOfStats {AGILITY, ENDURANCE, STRENGTH, MAGIC, LUCK}

    protected int strength;
    protected int endurance;
    protected int magic;
    protected int agility;
    protected int luck;

    protected int dodge;
    protected int block;
    protected int diceStrength;
    protected int diceQuantity;

    protected Quality quality;
    protected ArrayList<TypesOfStats> statsToChoose;

    public Item() {
    }

    protected Item(int playerLuck, int monsterLevel) {
        statsToChoose = new ArrayList<>();
        quality = determineQuality(playerLuck, monsterLevel);
        strength = 0;
        endurance = 0;
        magic = 0;
        agility = 0;
        luck = 0;
        dodge = 0;
        block = 0;
        diceStrength = 0;
        diceQuantity = 0;
    }

    private Quality determineQuality(int playerLuck, int monsterLevel) {
        Random rng = new Random();
        int normalChance = 260;
        int magicChance = 50 + (playerLuck + monsterLevel) * 2;
        int legendaryChance = 15 + playerLuck + monsterLevel;
        int seed = rng.nextInt(normalChance + magicChance + legendaryChance);
        if (seed < normalChance) {
            return Quality.NORMAL;
        } else if (seed >= normalChance && seed < normalChance + magicChance) {
            return Quality.MAGIC;
        } else {
            return Quality.LEGENDARY;
        }
    }

    protected void determineStats(int playerLuck, int monsterLevel) {
        int statsToSet = qualityToStatistics();
        Random rng = new Random();
        while (statsToSet > 0) {
            int index = rng.nextInt(statsToChoose.size());
            TypesOfStats stat = statsToChoose.get(index);
            statsToChoose.remove(index);
            switch (stat) {
                case AGILITY:
                    agility = setStatistic(playerLuck, monsterLevel);
                    break;
                case ENDURANCE:
                    endurance = setStatistic(playerLuck, monsterLevel);
                    break;
                case STRENGTH:
                    strength = setStatistic(playerLuck, monsterLevel);
                    break;
                case MAGIC:
                    magic = setStatistic(playerLuck, monsterLevel);
                    break;
                case LUCK:
                    luck = setStatistic(playerLuck, monsterLevel);
                    break;
            }
            statsToSet--;
        }
    }

    protected int qualityToStatistics() {
        if (quality == Quality.NORMAL) {
            return 1;
        } else if (quality == Quality.MAGIC) {
            return 2;
        } else {
            return 3;
        }
    }

    private int setStatistic(int playerLuck, int monsterLevel) {
        Random rng = new Random();
        int chance = 20 + playerLuck + monsterLevel;
        float value = (monsterLevel * 0.75f) * (0.1f + qualityToStatistics() * 0.1f);
        while (monsterLevel > 0) {
            if (rng.nextInt(100) < chance) {
                value += 1;
            }
            monsterLevel--;
        }
        return Math.round(value);
    }

    protected void updateItem(int strength, int endurance, int agility, int magic, int luck, Quality quality) {
        this.strength = strength;
        this.endurance = endurance;
        this.agility = agility;
        this.magic = magic;
        this.luck = luck;
        this.quality = quality;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getMagic() {
        return magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

    public int getDodge() {
        return dodge;
    }

    public int getBlock() {
        return block;
    }

    public int getDiceStrength() {
        return diceStrength;
    }

    public int getDiceQuantity() {
        return diceQuantity;
    }

    public Quality getQuality() {
        return quality;
    }
}
