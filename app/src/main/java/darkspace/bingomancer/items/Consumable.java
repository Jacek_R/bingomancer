package darkspace.bingomancer.items;

import java.util.Locale;

import darkspace.bingomancer.R;
import darkspace.bingomancer.activities.Game;
import darkspace.bingomancer.actors.Monster;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.utils.GameMusic;

public class Consumable extends Item {

    public enum Type {HEAL_POTION, MAGIC_POTION, SCROLL}

    private Type type;
    private int amount;

    public Consumable(Type type, int amount) {
        this.type = type;
        this.amount = amount;
    }

    public void use(Player player, Monster monster, Game game) {
        if (amount < 1) {
            game.updateMessages(game.getString(R.string.missingItem));
            return;
        }
        switch (type) {
            case HEAL_POTION:
                if (player.getHealth()[0] == player.getHealth()[1]) {
                    game.updateMessages(game.getString(R.string.fullHealth));
                    return;
                }
                player.getHealth()[0] = restoreStat(game, player.getHealth()[0], player.getHealth()[1], game.getString(R.string.usedHealingPotion), 0.15f);
                GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.POTION);
                break;
            case MAGIC_POTION:
                if (player.getMagicPower()[0] == player.getMagicPower()[1]) {
                    game.updateMessages(game.getString(R.string.fullMagic));
                    return;
                }
                player.getMagicPower()[0] = restoreStat(game, player.getMagicPower()[0], player.getMagicPower()[1], game.getString(R.string.usedMagicPotion), 0.2f);
                GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.POTION);
                break;
            case SCROLL:
                float damage = monster.getHealth()[1] * 0.15f;
                game.updateMessages(String.format(Locale.getDefault(), game.getString(R.string.usedScroll), (int) damage));
                GameMusic.getInstance().playSound(game.getApplicationContext(), GameMusic.BOMB);
                monster.getHealth()[0] -= (int) damage;
                break;
        }
        amount--;
    }

    private int restoreStat(Game game, int currentValue, int maxValue, String message, float mod) {
        int newValue = currentValue + (int) (maxValue * mod);
        game.updateMessages(String.format(Locale.getDefault(), message, newValue - currentValue));
        if (newValue > maxValue) {
            newValue = maxValue;
        }
        return newValue;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
