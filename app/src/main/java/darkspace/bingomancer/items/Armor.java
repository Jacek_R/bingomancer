package darkspace.bingomancer.items;

public class Armor extends Item {

    public Armor() {
        strength = 0;
        endurance = 0;
        agility = 0;
        magic = 0;
        luck = 0;
        quality = Quality.NORMAL;
        dodge = 0;
    }

    public Armor(int playerLuck, int monsterLevel) {
        super(playerLuck, monsterLevel);
        statsToChoose.add(TypesOfStats.ENDURANCE);
        statsToChoose.add(TypesOfStats.AGILITY);
        statsToChoose.add(TypesOfStats.LUCK);
        determineStats(playerLuck, monsterLevel);
        dodge = (monsterLevel / (4 - qualityToStatistics())) / 2;
        dodge++;
    }

    public void updateItem(int strength, int endurance, int agility, int magic, int luck, int dodge, Quality quality) {
        super.updateItem(strength, endurance, agility, magic, luck, quality);
        this.dodge = dodge;
    }
}
