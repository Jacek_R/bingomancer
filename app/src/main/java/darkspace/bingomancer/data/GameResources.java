package darkspace.bingomancer.data;

import darkspace.bingomancer.R;
import darkspace.bingomancer.actors.Monster;
import darkspace.bingomancer.bingo.Cell;
import darkspace.bingomancer.items.Armor;
import darkspace.bingomancer.items.Item;
import darkspace.bingomancer.items.Weapon;

public abstract class GameResources {

    public static int color(Cell.MagicColor color) {
        switch (color) {
            case BLUE:
                return R.drawable.border_blue;
            case RED:
                return R.drawable.border_red;
            case GREEN:
                return R.drawable.border_green;
            case NONE:
                return R.drawable.border_gray;
            default:
                return 0;
        }
    }

    public static int contentImage(Cell.Content content) {
        switch (content) {
            case NOTHING:
                return 0;
            case GOLD:
                return R.drawable.gold;
            case EXPERIENCE:
                return R.drawable.experience;
            case ITEM:
                return R.drawable.item;
            case POWER:
                return R.drawable.power;
            case HEAL:
                return R.drawable.heal;
            case CURSE:
                return R.drawable.curse;
            case POISON:
                return R.drawable.poison;
            case DAMAGE:
                return R.drawable.damage;
            default:
                return 0;
        }
    }

    public static int monsterImage(Monster.Type typeOfMonster) {
        switch (typeOfMonster) {
            case STRONG:
                return R.drawable.monster_strong;
            case RESISTANT:
                return R.drawable.monster_resistant;
            case AGILE:
                return R.drawable.monster_agile;
            case MAGICAL:
                return R.drawable.monster_magical;
            case CURSED:
                return R.drawable.monster_cursed;
            case POISONOUS:
                return R.drawable.monster_poisonous;
            case DAMAGE:
                return R.drawable.monster_damage;
            case BOSS:
                return R.drawable.monster_boss;
            default:
                return 0;
        }
    }

    public static int itemToDrawable(Item item) {
        if (item instanceof Weapon) {
            if (item.getQuality() == Item.Quality.NORMAL) {
                return R.drawable.weapon_normal;
            } else if (item.getQuality() == Item.Quality.MAGIC) {
                return R.drawable.weapon_magic;
            } else {
                return R.drawable.weapon_legendary;
            }
        } else if (item instanceof Armor) {
            if (item.getQuality() == Item.Quality.NORMAL) {
                return R.drawable.armor_normal;
            } else if (item.getQuality() == Item.Quality.MAGIC) {
                return R.drawable.armor_magic;
            } else {
                return R.drawable.armor_legendary;
            }
        } else {
            if (item.getQuality() == Item.Quality.NORMAL) {
                return R.drawable.shield_normal;
            } else if (item.getQuality() == Item.Quality.MAGIC) {
                return R.drawable.shield_magic;
            } else {
                return R.drawable.shield_legendary;
            }
        }
    }


}
