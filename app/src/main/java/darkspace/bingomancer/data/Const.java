package darkspace.bingomancer.data;

public class Const {
    public static final int CARD_SIDE = 5;
    public static final int NUMBER_GROUP = 15;
    public final static int NUMBER_OF_SKILLS = 9;

    public static final int RESUME_FIGHT = 1;
}
