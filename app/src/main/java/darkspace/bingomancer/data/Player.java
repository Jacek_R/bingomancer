package darkspace.bingomancer.data;

import java.io.Serializable;
import java.util.HashMap;

import darkspace.bingomancer.bingo.Cell;
import darkspace.bingomancer.gameplay.Skill;
import darkspace.bingomancer.items.Armor;
import darkspace.bingomancer.items.Consumable;
import darkspace.bingomancer.items.Shield;
import darkspace.bingomancer.items.Weapon;

public class Player implements Serializable {

    private int strength;
    private int agility;
    private int luck;
    private int endurance;
    private int magic;

    private int[] health;
    private int[] magicPower;
    private int[] experience;

    private boolean poisoned;
    private boolean patienceActive;
    private boolean defenderActive;

    private int level;
    private int monsterLevel;
    private int gold;

    private Cell.MagicColor strongColor;
    private Cell.MagicColor weakColor;

    private HashMap<Skill.Name, Boolean> skills;

    private Shield shield;
    private Armor armor;
    private Weapon weapon;
    private Consumable healingPotion;
    private Consumable magicPotion;
    private Consumable bomb;

    public Player(int strength, int agility, int luck, int endurance, int magic, Cell.MagicColor strongColor, Cell.MagicColor weakColor, Skill.Name startingSkill) {
        createDefaultSkillList();
        createDefaultItems();
        this.strength = strength;
        this.agility = agility;
        this.luck = luck;
        this.endurance = endurance;
        this.magic = magic;
        this.strongColor = strongColor;
        this.weakColor = weakColor;
        level = 1;
        monsterLevel = 1;
        poisoned = false;
        defenderActive = false;
        patienceActive = false;
        gold = 0;
        skills.put(startingSkill, true);

        int maxHealth = maximumHealth();
        int maxMagicPower = maximumMagicPower();

        health = new int[]{maxHealth, maxHealth};
        magicPower = new int[]{maxMagicPower, maxMagicPower};
        experience = new int[]{0, toNextLevel()};

    }

    private void createDefaultItems() {
        shield = new Shield();
        armor = new Armor();
        weapon = new Weapon();
        healingPotion = new Consumable(Consumable.Type.HEAL_POTION, 1);
        magicPotion = new Consumable(Consumable.Type.MAGIC_POTION, 1);
        bomb = new Consumable(Consumable.Type.SCROLL, 1);
    }

    public int maximumMagicPower() {
        return magic * 3 + level * 3;
    }

    public int toNextLevel() {
        return level * 75;
    }

    public int maximumHealth() {
        return strength + endurance * 4 + level * 8;
    }

    private void createDefaultSkillList() {
        skills = new HashMap<>();
        skills.put(Skill.Name.DEFENDER, false);
        skills.put(Skill.Name.FREE_TAP, false);
        skills.put(Skill.Name.GOLD_RAIN, false);
        skills.put(Skill.Name.HEAL, false);
        skills.put(Skill.Name.MANA_RAIN, false);
        skills.put(Skill.Name.PATIENCE, false);
        skills.put(Skill.Name.RAGE, false);
        skills.put(Skill.Name.SCAVENGE, false);
        skills.put(Skill.Name.WISDOM, false);
    }

    public int getStrength() {
        return strength;
    }

    public int getTotalStrength() {
        return strength + weapon.getStrength() + armor.getStrength() + shield.getStrength();
    }

    public int getTotalAgility() {
        return agility + weapon.getAgility() + armor.getAgility() + shield.getAgility();
    }

    public int getTotalLuck() {
        return luck + weapon.getLuck() + armor.getLuck() + shield.getLuck();
    }

    public int getTotalMagic() {
        return magic + weapon.getMagic() + armor.getMagic() + shield.getMagic();
    }

    public int getTotalEndurance() {
        return endurance + weapon.getEndurance() + armor.getEndurance() + shield.getEndurance();
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getMagic() {
        return magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    public int[] getHealth() {
        return health;
    }

    public int[] getMagicPower() {
        return magicPower;
    }

    public int[] getExperience() {
        return experience;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public boolean isPoisoned() {
        return poisoned;
    }

    public void setPoisoned(boolean poisoned) {
        this.poisoned = poisoned;
    }

    public int getMonsterLevel() {
        return monsterLevel;
    }

    public void setMonsterLevel(int monsterLevel) {
        this.monsterLevel = monsterLevel;
    }

    public Cell.MagicColor getStrongColor() {
        return strongColor;
    }

    public HashMap<Skill.Name, Boolean> getSkills() {
        return skills;
    }

    public boolean isPatienceActive() {
        return patienceActive;
    }

    public void setPatienceActive(boolean patienceActive) {
        this.patienceActive = patienceActive;
    }

    public boolean isDefenderActive() {
        return defenderActive;
    }

    public void setDefenderActive(boolean defenderActive) {
        this.defenderActive = defenderActive;
    }

    public Cell.MagicColor getWeakColor() {
        return weakColor;
    }

    public Shield getShield() {
        return shield;
    }

    public Armor getArmor() {
        return armor;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Consumable getHealingPotion() {
        return healingPotion;
    }

    public Consumable getMagicPotion() {
        return magicPotion;
    }

    public Consumable getBomb() {
        return bomb;
    }
}
