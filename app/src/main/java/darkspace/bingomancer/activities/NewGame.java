package darkspace.bingomancer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import darkspace.bingomancer.R;
import darkspace.bingomancer.bingo.Cell;
import darkspace.bingomancer.builders.GameOverDialog;
import darkspace.bingomancer.builders.ToastBuilder;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.gameplay.Skill;
import darkspace.bingomancer.utils.AnimationNew;
import darkspace.bingomancer.utils.DataManager;
import darkspace.bingomancer.utils.GameMusic;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NewGame extends AppCompatActivity {

    @BindView(R.id.strengthValue)
    TextView strengthText;
    @BindView(R.id.enduranceValue)
    TextView enduranceText;
    @BindView(R.id.magicValue)
    TextView magicText;
    @BindView(R.id.agilityValue)
    TextView agilityText;
    @BindView(R.id.luckValue)
    TextView luckText;
    @BindView(R.id.pointsValue)
    TextView remainText;

    @BindView(R.id.selectDefender)
    FrameLayout defender;
    @BindView(R.id.selectFreeTap)
    FrameLayout freeTap;
    @BindView(R.id.selectGoldRain)
    FrameLayout goldRain;
    @BindView(R.id.selectHeal)
    FrameLayout heal;
    @BindView(R.id.selectMagicChannel)
    FrameLayout magicChannel;
    @BindView(R.id.selectPatience)
    FrameLayout patience;
    @BindView(R.id.selectRage)
    FrameLayout rage;
    @BindView(R.id.selectScavenger)
    FrameLayout scavenger;
    @BindView(R.id.selectWisdom)
    FrameLayout wisdom;

    private int pointsLeft;
    private boolean skillWasPicked;

    private Skill.Name selectedSkill;

    private int strength;
    private int endurance;
    private int agility;
    private int magic;
    private int luck;

    private FrameLayout[] skills;

    private final int minimalValue = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_game);
        ButterKnife.bind(this);
        setInitialVariables();
        putSkillTextViewsIntoArray();
        GameMusic.activities_open++;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameMusic.getInstance().continuePlaying();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing()) {
            GameMusic.getInstance().pauseMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GameMusic.activities_open--;
        GameMusic.getInstance().destroyMusic();
    }

    @Override
    public void onBackPressed() {
        new GameOverDialog().show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void putSkillTextViewsIntoArray() {
        skills = new FrameLayout[]{defender, freeTap, goldRain, heal, magicChannel, patience, rage, scavenger, wisdom};
    }

    private void setInitialVariables() {
        pointsLeft = 10;
        skillWasPicked = false;
        selectedSkill = Skill.Name.UNKNOWN;

        strength = minimalValue;
        endurance = minimalValue;
        agility = minimalValue;
        magic = minimalValue;
        luck = minimalValue;
        setValuesInTextViews();
    }

    private void setValuesInTextViews() {
        strengthText.setText(String.valueOf(strength));
        enduranceText.setText(String.valueOf(endurance));
        agilityText.setText(String.valueOf(agility));
        magicText.setText(String.valueOf(magic));
        luckText.setText(String.valueOf(luck));
        remainText.setText(String.valueOf(pointsLeft));
    }

    private int minusStat(TextView textView, int currentValue) {
        if (currentValue - 1 < minimalValue) {
            ToastBuilder.create(getString(R.string.minimumReached), this).show();
            return 0;
        } else {
            textView.setText(String.valueOf(currentValue - 1));
            pointsLeft++;
            remainText.setText(String.valueOf(pointsLeft));
            return 1;
        }
    }

    private int plusStat(TextView textView, int currentValue) {
        final int maximumValue = 10;
        if (currentValue + 1 > maximumValue || pointsLeft == 0) {
            if (currentValue == 10) {
                ToastBuilder.create(getString(R.string.maximumSkillValue), this).show();
            } else {
                ToastBuilder.create(getString(R.string.pointsSpent), this).show();
            }
            return 0;
        } else {
            textView.setText(String.valueOf(currentValue + 1));
            pointsLeft--;
            remainText.setText(String.valueOf(pointsLeft));
            return 1;
        }
    }

    private void pickSkill(FrameLayout layoutToEnable, Skill.Name pickedSkillName) {
        for (FrameLayout layout : skills) {
            layout.setEnabled(false);
        }
        layoutToEnable.setEnabled(true);
        selectedSkill = pickedSkillName;
        skillWasPicked = true;
    }

    @OnClick({R.id.selectDefender, R.id.selectFreeTap, R.id.selectGoldRain, R.id.selectHeal,
            R.id.selectMagicChannel, R.id.selectPatience, R.id.selectRage, R.id.selectScavenger, R.id.selectWisdom})
    public void selectSkill(View view) {
        switch (view.getId()) {
            case R.id.selectDefender:
                pickSkill(defender, Skill.Name.DEFENDER);
                break;
            case R.id.selectFreeTap:
                pickSkill(freeTap, Skill.Name.FREE_TAP);
                break;
            case R.id.selectGoldRain:
                pickSkill(goldRain, Skill.Name.GOLD_RAIN);
                break;
            case R.id.selectHeal:
                pickSkill(heal, Skill.Name.HEAL);
                break;
            case R.id.selectMagicChannel:
                pickSkill(magicChannel, Skill.Name.MANA_RAIN);
                break;
            case R.id.selectPatience:
                pickSkill(patience, Skill.Name.PATIENCE);
                break;
            case R.id.selectRage:
                pickSkill(rage, Skill.Name.RAGE);
                break;
            case R.id.selectScavenger:
                pickSkill(scavenger, Skill.Name.SCAVENGE);
                break;
            case R.id.selectWisdom:
                pickSkill(wisdom, Skill.Name.WISDOM);
                break;
        }
    }

    @OnClick({R.id.plusAgility, R.id.plusEndurace, R.id.plusLuck, R.id.plusMagic, R.id.plusStrength,
            R.id.minusAgility, R.id.minusEndurance, R.id.minusLuck, R.id.minusMagic, R.id.minusStrength})
    public void modifyStat(Button view) {
        switch (view.getId()) {
            case R.id.minusAgility:
                agility -= minusStat(agilityText, agility);
                break;
            case R.id.minusEndurance:
                endurance -= minusStat(enduranceText, endurance);
                break;
            case R.id.minusLuck:
                luck -= minusStat(luckText, luck);
                break;
            case R.id.minusMagic:
                magic -= minusStat(magicText, magic);
                break;
            case R.id.minusStrength:
                strength -= minusStat(strengthText, strength);
                break;
            case R.id.plusAgility:
                agility += plusStat(agilityText, agility);
                break;
            case R.id.plusEndurace:
                endurance += plusStat(enduranceText, endurance);
                break;
            case R.id.plusLuck:
                luck += plusStat(luckText, luck);
                break;
            case R.id.plusMagic:
                magic += plusStat(magicText, magic);
                break;
            case R.id.plusStrength:
                strength += plusStat(strengthText, strength);
                break;
        }
    }

    @OnClick(R.id.reset)
    public void reset() {
        setInitialVariables();
        for (FrameLayout layout : skills) {
            layout.setEnabled(true);
        }
    }

    @OnClick(R.id.acceptProf)
    public void accept() {
        if (pointsLeft == 0 && skillWasPicked) {
            Intent intent = new Intent(this, Game.class);
            Player player = new Player(strength, agility, luck, endurance, magic, Cell.MagicColor.RED, Cell.MagicColor.BLUE, selectedSkill);
            intent.putExtra(Game.KEY_FOR_NEW_PLAYER, DataManager.createJson(player));
            startActivity(intent, AnimationNew.activityTransition(this).toBundle());
            finish();
        } else if (pointsLeft != 0) {
            ToastBuilder.create(getString(R.string.pointsLeft), this).show();
        } else {
            ToastBuilder.create(getString(R.string.skillNotSelected), this).show();
        }
    }
}
