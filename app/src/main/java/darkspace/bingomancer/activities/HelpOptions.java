package darkspace.bingomancer.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import darkspace.bingomancer.R;
import darkspace.bingomancer.utils.AnimationNew;
import darkspace.bingomancer.utils.GameMusic;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HelpOptions extends AppCompatActivity {

    @BindView(R.id.toggleSoundBtn)
    ToggleButton toggleSound;
    @BindView(R.id.toggleMusicBtn)
    ToggleButton toggleMusic;

    @BindView(R.id.help_bingoPatterns)
    RelativeLayout bingoPatterns;
    @BindView(R.id.help_cellContent)
    RelativeLayout cellContent;
    @BindView(R.id.help_gameplay)
    RelativeLayout gamePlay;
    @BindView(R.id.help_gameScreen)
    RelativeLayout gameScreen;
    @BindView(R.id.help_items)
    RelativeLayout items;
    @BindView(R.id.help_monsters)
    RelativeLayout monsters;
    @BindView(R.id.help_overview)
    RelativeLayout overview;
    @BindView(R.id.help_skills)
    RelativeLayout skills;
    @BindView(R.id.help_statistics)
    RelativeLayout statistics;

    ArrayList<RelativeLayout> layouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_options);
        ButterKnife.bind(this);
        setButtons(this);
        setLayouts();
        GameMusic.activities_open++;
    }

    private void setLayouts() {
        layouts = new ArrayList<>();
        layouts.add(bingoPatterns);
        layouts.add(cellContent);
        layouts.add(gamePlay);
        layouts.add(gameScreen);
        layouts.add(items);
        layouts.add(monsters);
        layouts.add(overview);
        layouts.add(skills);
        layouts.add(statistics);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent, AnimationNew.activityTransition(this).toBundle());
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing()) {
            GameMusic.getInstance().pauseMusic();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameMusic.getInstance().continuePlaying();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GameMusic.activities_open--;
        GameMusic.getInstance().destroyMusic();
    }

    @OnClick({R.id.btn_bingoPatterns, R.id.btn_gameplay, R.id.btn_cellsContent, R.id.btn_gameScreen,
            R.id.btn_items, R.id.btn_monsters, R.id.btn_overview, R.id.btn_skillAndItems, R.id.btn_statistics})
    public void showLayout(View button) {
        for (RelativeLayout layout : layouts) {
            layout.setVisibility(View.INVISIBLE);
        }
        switch (button.getId()) {
            case R.id.btn_bingoPatterns:
                bingoPatterns.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_gameplay:
                gamePlay.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_cellsContent:
                cellContent.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_gameScreen:
                gameScreen.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_items:
                items.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_monsters:
                monsters.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_overview:
                overview.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_skillAndItems:
                skills.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_statistics:
                statistics.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setButtons(final Context context) {
        if (GameMusic.musicOn) {
            toggleMusic.setChecked(true);
        } else {
            toggleMusic.setChecked(false);
        }
        if (GameMusic.soundsOn) {
            toggleSound.setChecked(true);
        } else {
            toggleSound.setChecked(false);
        }
        toggleMusic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                GameMusic.musicOn = !GameMusic.musicOn;
                if (isChecked) {
                    GameMusic.getInstance().loadTrack(context, GameMusic.MENU);
                } else {
                    GameMusic.getInstance().stopMusic();
                }
                saveToggle();
            }
        });

        toggleSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                GameMusic.soundsOn = !GameMusic.soundsOn;
                saveToggle();
            }
        });
    }

    public void toggleMusic(View view) {
        GameMusic.musicOn = !GameMusic.musicOn;
        if (GameMusic.musicOn) {
            GameMusic.getInstance().loadTrack(this, GameMusic.MENU);
        } else {
            GameMusic.getInstance().stopMusic();
        }
        saveToggle();
    }

    public void toggleSound(View view) {
        GameMusic.soundsOn = !GameMusic.soundsOn;
        saveToggle();
    }

    private void saveToggle() {
        SharedPreferences pref = getSharedPreferences(GameMusic.MUSIC_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(GameMusic.MUSIC_KEY, GameMusic.musicOn);
        editor.putBoolean(GameMusic.SOUND_KEY, GameMusic.soundsOn);
        editor.apply();
    }

}
