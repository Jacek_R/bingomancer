package darkspace.bingomancer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import darkspace.bingomancer.R;
import darkspace.bingomancer.builders.GameOverDialog;
import darkspace.bingomancer.builders.ToastBuilder;
import darkspace.bingomancer.data.Const;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.utils.AnimationNew;
import darkspace.bingomancer.utils.GameMusic;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Summary extends AppCompatActivity {

    public static final String EXPERIENCE_REWARD = "ExperienceReward";
    public static final String GOLD_REWARD = "GoldReward";
    public static final String ITEM_REWARD = "ItemReward";
    public static final String LEVEL_UP = "LevelsRaised";
    public static final String PLAYER = "PlayerObject";
    private static final int HEAL_HP_COST = 1;
    private static final int HEAL_MP_COST = 3;

    @BindView(R.id.tvExperienceBonus)
    TextView experience;
    @BindView(R.id.tvGoldBonus)
    TextView gold;
    @BindView(R.id.tvItemBonus)
    TextView item;
    @BindView(R.id.tvLevelUp)
    TextView level;
    @BindView(R.id.healHealthMaxValue)
    TextView healthMax;
    @BindView(R.id.healHealthValue)
    TextView healthValue;
    @BindView(R.id.healMagicPowerMaxValue)
    TextView magicPowerMax;
    @BindView(R.id.healMagicPowerValue)
    TextView magicPowerValue;
    @BindView(R.id.healGoldAmount)
    TextView goldAmount;

    private int newLevels;
    private int items;
    private Player player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);
        ButterKnife.bind(this);

        experience.setText(String.format(Locale.getDefault(), getString(R.string.experienceBonus), getIntent().getIntExtra(EXPERIENCE_REWARD, 0)));
        gold.setText(String.format(Locale.getDefault(), getString(R.string.goldBonus), getIntent().getIntExtra(GOLD_REWARD, 0)));
        item.setText(String.format(Locale.getDefault(), getString(R.string.itemBonus), getIntent().getIntExtra(ITEM_REWARD, 0)));
        newLevels = getIntent().getIntExtra(LEVEL_UP, 0);
        items = getIntent().getIntExtra(ITEM_REWARD, 0);
        player = ((Player) getIntent().getSerializableExtra(PLAYER));

        healthValue.setText(String.valueOf(player.getHealth()[0]));
        healthMax.setText(String.valueOf(player.getHealth()[1]));
        magicPowerValue.setText(String.valueOf(player.getMagicPower()[0]));
        magicPowerMax.setText(String.valueOf(player.getMagicPower()[1]));
        goldAmount.setText(String.format(Locale.getDefault(), getString(R.string.howManyGold), player.getGold()));

        if (newLevels == 0) {
            level.setVisibility(View.GONE);
        } else {
            level.setVisibility(View.VISIBLE);
        }
        GameMusic.activities_open++;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing()) {
            GameMusic.getInstance().pauseMusic();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameMusic.getInstance().continuePlaying();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GameMusic.activities_open--;
        GameMusic.getInstance().destroyMusic();
    }

    @Override
    public void onBackPressed() {
        new GameOverDialog().show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void healOne(View view) {
        if (view.getId() == R.id.healthHealOne) {
            addOne(player.getHealth(), HEAL_HP_COST, healthValue);
        } else if (view.getId() == R.id.magicPowerHealOne) {
            addOne(player.getMagicPower(), HEAL_MP_COST, magicPowerValue);
        }
    }

    public void healMax(View view) {
        if (view.getId() == R.id.healthHealMax) {
            addMax(player.getHealth(), HEAL_HP_COST, healthValue);
        } else if (view.getId() == R.id.magicPowerHealMax) {
            addMax(player.getMagicPower(), HEAL_MP_COST, magicPowerValue);
        }
    }

    private void addOne(int[] statToHeal, int cost, TextView stat) {
        if (cost > player.getGold()) {
            ToastBuilder.create(getString(R.string.insufficientGold), this).show();
        } else {
            if (statToHeal[0] == statToHeal[1]) {
                ToastBuilder.create(getString(R.string.maximumReached), this).show();
            } else {
                statToHeal[0]++;
                player.setGold(player.getGold() - cost);
                stat.setText(String.valueOf(statToHeal[0]));
                goldAmount.setText(String.format(Locale.getDefault(), getString(R.string.howManyGold), player.getGold()));
            }
        }
    }

    private void addMax(int[] statToHeal, int cost, TextView stat) {
        if (statToHeal[0] == statToHeal[1]) {
            ToastBuilder.create(getString(R.string.maximumReached), this).show();
            return;
        }
        int costOfHealing = (statToHeal[1] - statToHeal[0]) * cost;
        if (costOfHealing > player.getGold()) {
            int howManyStatCanHeal = player.getGold() / cost;
            if (howManyStatCanHeal == 0) {
                return;
            }
            statToHeal[0] += howManyStatCanHeal;
            stat.setText(String.valueOf(statToHeal[0]));
            player.setGold(player.getGold() - howManyStatCanHeal * cost);
            goldAmount.setText(String.format(Locale.getDefault(), getString(R.string.howManyGold), player.getGold()));
        } else {
            statToHeal[0] = statToHeal[1];
            stat.setText(String.valueOf(statToHeal[0]));
            player.setGold(player.getGold() - costOfHealing);
            goldAmount.setText(String.format(Locale.getDefault(), getString(R.string.howManyGold), player.getGold()));
        }
    }

    public void nextScreen(View view) {
        if (items > 0) {
            Intent intent = new Intent(this, CollectItem.class);
            intent.putExtra(PLAYER, player);
            intent.putExtra(LEVEL_UP, newLevels);
            intent.putExtra(ITEM_REWARD, items);
            startActivity(intent, AnimationNew.activityTransition(this).toBundle());
            finish();
        } else if (newLevels > 0) {
            Intent intent = new Intent(this, LevelUp.class);
            intent.putExtra(PLAYER, player);
            intent.putExtra(LEVEL_UP, newLevels);
            startActivity(intent, AnimationNew.activityTransition(this).toBundle());
            finish();
        } else {
            EventBus.getDefault().post(Const.RESUME_FIGHT);
            finish();
        }
    }
}
