package darkspace.bingomancer.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.lb.auto_fit_textview.AutoResizeTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import darkspace.bingomancer.R;
import darkspace.bingomancer.actors.Monster;
import darkspace.bingomancer.bingo.Card;
import darkspace.bingomancer.bingo.Draw;
import darkspace.bingomancer.builders.GameActivityViews;
import darkspace.bingomancer.builders.GameOverDialog;
import darkspace.bingomancer.builders.ImageViewBuilder;
import darkspace.bingomancer.builders.LinearLayoutBuilder;
import darkspace.bingomancer.builders.RelativeLayoutBuilder;
import darkspace.bingomancer.builders.TextViewBuilder;
import darkspace.bingomancer.data.Const;
import darkspace.bingomancer.data.GameResources;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.gameplay.ContentHandler;
import darkspace.bingomancer.gameplay.Fight;
import darkspace.bingomancer.gameplay.Skill;
import darkspace.bingomancer.items.Armor;
import darkspace.bingomancer.items.Shield;
import darkspace.bingomancer.items.Weapon;
import darkspace.bingomancer.utils.AnimationNew;
import darkspace.bingomancer.utils.DataManager;
import darkspace.bingomancer.utils.GameMusic;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Game extends AppCompatActivity {

    @BindView(R.id.monsterAgilityValue)
    TextView monAgi;
    @BindView(R.id.monsterEnduranceValue)
    TextView monEnd;
    @BindView(R.id.monsterStrengthValue)
    TextView monStr;
    @BindView(R.id.monsterMagicValue)
    TextView monMag;
    @BindView(R.id.monsterHpCurrent)
    TextView monHp;
    @BindView(R.id.monsterHpMax)
    TextView monHpMax;
    @BindView(R.id.minDamageMonster)
    TextView minDamageMonster;
    @BindView(R.id.maxDamageMonster)
    TextView maxDamageMonster;

    @BindView(R.id.playerAgilityValue)
    TextView plaAgi;
    @BindView(R.id.playerStrengthValue)
    TextView plaStr;
    @BindView(R.id.playerLuckValue)
    TextView plaLck;
    @BindView(R.id.playerMagicValue)
    TextView plaMag;
    @BindView(R.id.playerEnduranceValue)
    TextView plaEnd;
    @BindView(R.id.playerHpCurrent)
    TextView plaHp;
    @BindView(R.id.playerHpMax)
    TextView plaHpMax;
    @BindView(R.id.playerMpCurrent)
    TextView plaMp;
    @BindView(R.id.playerMpMax)
    TextView plaMpMax;
    @BindView(R.id.playerDmgMinimal)
    TextView plaDmgMin;
    @BindView(R.id.playerDmgMaximum)
    TextView plaDmgMax;

    @BindView(R.id.costDefender)
    TextView costDefender;
    @BindView(R.id.costFreeTap)
    TextView costFreeTap;
    @BindView(R.id.costGoldRain)
    TextView costGoldRain;
    @BindView(R.id.costHeal)
    TextView costHeal;
    @BindView(R.id.costMagicChannel)
    TextView costMagicChannel;
    @BindView(R.id.costPatience)
    TextView costPatience;
    @BindView(R.id.costRage)
    TextView costRage;
    @BindView(R.id.costScavenger)
    TextView costScavenger;
    @BindView(R.id.costWisdom)
    TextView costWisdom;
    @BindView(R.id.amountHealPotions)
    TextView amountHealPotions;
    @BindView(R.id.amountMagicPotions)
    TextView amountMagicPotions;
    @BindView(R.id.amountBombs)
    TextView amountBombs;

    @BindView(R.id.useDefender)
    FrameLayout useDefender;
    @BindView(R.id.useFreeTap)
    FrameLayout useFreeTap;
    @BindView(R.id.useGoldRain)
    FrameLayout useGoldRain;
    @BindView(R.id.useHeal)
    FrameLayout useHeal;
    @BindView(R.id.useMagicChannel)
    FrameLayout useMagicChannel;
    @BindView(R.id.usePatience)
    FrameLayout usePatience;
    @BindView(R.id.useRage)
    FrameLayout useRage;
    @BindView(R.id.useScavenger)
    FrameLayout useScavenger;
    @BindView(R.id.useWisdom)
    FrameLayout useWisdom;
    @BindView(R.id.useHealingPotion)
    FrameLayout useHealingPotion;
    @BindView(R.id.useMagicPotion)
    FrameLayout useMagicPotion;
    @BindView(R.id.useBomb)
    FrameLayout useBomb;

    @BindView(R.id.monsterImage)
    ImageView monsterImage;
    @BindView(R.id.playerLevelValue)
    TextView playerLevel;
    @BindView(R.id.monsterName)
    TextView monsterName;
    @BindView(R.id.monsterLevelValue)
    TextView monsterLevel;
    @BindView(R.id.goldValue)
    TextView gold;
    @BindView(R.id.experienceCurrent)
    TextView experience;
    @BindView(R.id.experienceGoal)
    TextView experienceGoal;

    @BindView(R.id.card)
    LinearLayout grid;
    @BindView(R.id.drawnNumbers)
    LinearLayout drawnNumbers;

    @BindView(R.id.lastDraw)
    TextView lastDraw;
    @BindView(R.id.secondToLastDraw)
    TextView secondLast;
    @BindView(R.id.thirdToLastView)
    TextView thirdLast;
    @BindView(R.id.messageLog)
    ScrollView log;

    @BindView(R.id.messagesView)
    TextView messageLog;

    @BindDimen(R.dimen.mediumMargin)
    int margin;
    @BindDimen(R.dimen.smallMargin)
    int smallMargin;

    private Draw draw;
    private Card card;
    private Player player;
    private Monster monster;
    private ContentHandler contentHandler;
    private Fight fight;
    private Skill skill;

    private boolean pause = false;
    private boolean fightIsOver = false;
    private boolean popUpInitiated;

    private long counterStartTime;
    private long counterStopTime;
    private long drawInterval;

    private int attackCounter;

    private Handler handler;
    private ArrayList<AutoResizeTextView> numbersTextViews;
    private ArrayList<TextView> lastDraws;

    public final static String PREFERENCES_KEY = "ThereIsASavedGameForThisAwesomeGame";
    public final static String PLAYER_KEY = "SomewhereIsAPlayer";
    public final static String MONSTER_KEY = "SomewhereIsAMonster";
    public final static String KEY_FOR_NEW_PLAYER = "NewPlayerWasCreated";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        GameMusic.getInstance().loadTrack(this, GameMusic.GAME);
        boolean continueGame = getIntent().getBooleanExtra(MainActivity.LOAD_GAME, false);
        createPlayerAndMonster(continueGame, getIntent().getStringExtra(KEY_FOR_NEW_PLAYER));
        handler = new Handler();
        card = new Card(player.getMonsterLevel(), monster.getCurseTiles(), monster.getDamageTiles(), monster.getPoisonTiles());
        draw = new Draw(player, card);
        skill = new Skill(this, player, card);
        drawInterval = monster.checkInterval(player.getTotalAgility());
        setArrays();
        initiateInfoInView();
        createGrid();
        GameActivityViews.createDrawnNumbers(this, drawnNumbers, numbersTextViews, margin);
        setDrawingMachine(drawInterval, drawInterval);
        setSkillButtons();
        messageLog.setText(R.string.tapHere);
        contentHandler = new ContentHandler(this, player, monster);
        fight = new Fight(this, player, monster);
        attackCounter = monster.getAttackCounter();
        GameMusic.activities_open++;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameMusic.getInstance().continuePlaying();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing() && !fightIsOver) {
            if (!pause) {
                pauseDraw();
            }
            GameMusic.getInstance().pauseMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
        GameMusic.activities_open--;
        GameMusic.getInstance().destroyMusic();
    }

    @Override
    public void onBackPressed() {
        new GameOverDialog().show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Subscribe
    public void backFromSummary(Integer actionId) {
        if (actionId == Const.RESUME_FIGHT) {
            createNewFight();
        }
    }

    @Subscribe
    public void updatePlayerStats(Player player) {
        this.player.setStrength(player.getStrength());
        this.player.setAgility(player.getAgility());
        this.player.setEndurance(player.getEndurance());
        this.player.setMagic(player.getMagic());
        this.player.setLuck(player.getLuck());
        this.player.getHealth()[0] = player.getHealth()[0];
        this.player.getHealth()[1] = player.getHealth()[1];
        this.player.getMagicPower()[0] = player.getMagicPower()[0];
        this.player.getMagicPower()[1] = player.getMagicPower()[1];
        this.player.setGold(player.getGold());
        this.player.getSkills().put(Skill.Name.DEFENDER, player.getSkills().get(Skill.Name.DEFENDER));
        this.player.getSkills().put(Skill.Name.FREE_TAP, player.getSkills().get(Skill.Name.FREE_TAP));
        this.player.getSkills().put(Skill.Name.GOLD_RAIN, player.getSkills().get(Skill.Name.GOLD_RAIN));
        this.player.getSkills().put(Skill.Name.HEAL, player.getSkills().get(Skill.Name.HEAL));
        this.player.getSkills().put(Skill.Name.MANA_RAIN, player.getSkills().get(Skill.Name.MANA_RAIN));
        this.player.getSkills().put(Skill.Name.PATIENCE, player.getSkills().get(Skill.Name.PATIENCE));
        this.player.getSkills().put(Skill.Name.RAGE, player.getSkills().get(Skill.Name.RAGE));
        this.player.getSkills().put(Skill.Name.SCAVENGE, player.getSkills().get(Skill.Name.SCAVENGE));
        this.player.getSkills().put(Skill.Name.WISDOM, player.getSkills().get(Skill.Name.WISDOM));
        this.player.getBomb().setAmount(player.getBomb().getAmount());
        this.player.getMagicPotion().setAmount(player.getMagicPotion().getAmount());
        this.player.getHealingPotion().setAmount(player.getHealingPotion().getAmount());
        updateItems(player);
        createNewFight();
    }

    private void updateItems(Player player) {
        Weapon weapon = player.getWeapon();
        Armor armor = player.getArmor();
        Shield shield = player.getShield();
        this.player.getWeapon().updateItem(
                weapon.getStrength(), weapon.getEndurance(), weapon.getAgility(), weapon.getMagic(),
                weapon.getLuck(), weapon.getDiceStrength(), weapon.getDiceQuantity(), weapon.getQuality());
        this.player.getShield().updateItem(
                shield.getStrength(), shield.getEndurance(), shield.getAgility(), shield.getMagic(),
                shield.getLuck(), shield.getBlock(), shield.getQuality()
        );
        this.player.getArmor().updateItem(armor.getStrength(), armor.getEndurance(), armor.getAgility(),
                armor.getMagic(), armor.getLuck(), armor.getDodge(), armor.getQuality()
        );
    }

    public void createNewFight() {
        fightNextMonster();
        createNewCard();
        saveGame();
    }

    private void fightNextMonster() {
        fightIsOver = false;
        drawInterval = monster.checkInterval(player.getAgility());
        initiateInfoInView();
        setSkillButtons();
        attackCounter = monster.getAttackCounter();
    }

    private void createNewCard() {
        card.createNewCard(player.getMonsterLevel(), monster.getCurseTiles(), monster.getDamageTiles(), monster.getPoisonTiles());
        draw.createNewDraw();
        createGrid();
        GameActivityViews.createDrawnNumbers(this, drawnNumbers, numbersTextViews, margin);
        setDrawingMachine(drawInterval, drawInterval);
        for (TextView textView : lastDraws) {
            textView.setText("");
        }
        messageLog.setText(R.string.tapHere);
    }

    private void createPlayerAndMonster(boolean continueGame, String newPlayerJson) {
        if (continueGame) {
            SharedPreferences pref = this.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
            player = DataManager.createPlayerFromJson(pref.getString(PLAYER_KEY, ""));
            monster = DataManager.createMonsterFromJson(pref.getString(MONSTER_KEY, ""));
        } else {
            player = DataManager.createPlayerFromJson(newPlayerJson);
            monster = new Monster(player.getMonsterLevel(), this);
            saveGame();
        }
        popUpInitiated = false;
    }

    private void saveGame() {
        SharedPreferences pref = this.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PLAYER_KEY, DataManager.createJson(player));
        editor.putString(MONSTER_KEY, DataManager.createJson(monster));
        editor.apply();
    }

    private void setDrawingMachine(final long interval, long initialCountdown) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (draw.getRemainingNumbers().size() == 0) {
                    return;
                }
                counterStartTime = System.currentTimeMillis();
                drawNumber();
                attackCounter--;
                if (attackCounter == 0 && monster.getHealth()[0] > 0) {
                    fight.playerIsAttacked();
                    attackCounter = monster.getAttackCounter();
                    testOfDeath();
                }
                if (!fight.someoneIsDead() && draw.getRemainingNumbers().size() != 0) {
                    handler.postDelayed(this, interval);
                } else if (draw.getRemainingNumbers().size() != 0 && !popUpInitiated) {
                    testOfDeath();
                }
            }
        };
        handler.postDelayed(runnable, initialCountdown);
    }

    private void createGrid() {
        grid.removeAllViews();
        int row = 0;
        int column = 0;
        while (row < Const.CARD_SIDE) {
            int weight = 1;
            float weightSum = Const.CARD_SIDE;
            LinearLayout.LayoutParams parentParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, weight);
            LinearLayout parent = LinearLayoutBuilder.createLinearLayout(this, parentParams, weightSum, LinearLayout.HORIZONTAL);

            while (column < Const.CARD_SIDE) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        0, ViewGroup.LayoutParams.MATCH_PARENT, weight);
                params.setMargins(smallMargin, smallMargin, smallMargin, smallMargin);
                params.gravity = Gravity.CENTER;

                RelativeLayout child = RelativeLayoutBuilder.createRelativeLayout(
                        this, params, GameResources.color(card.getCells()[row][column].getColor()));

                ImageView image = ImageViewBuilder.createImage(
                        this, GameResources.contentImage(card.getCells()[row][column].getContent()));

                image.setAlpha(0.35f);
                child.addView(image);
                card.getCells()[row][column].setImageOfACell(image);

                String message = String.valueOf(card.getCells()[row][column].getNumber());
                RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                TextView textView = TextViewBuilder.createText(
                        this, message, textParams, Gravity.CENTER, Color.BLACK, Typeface.BOLD);
                child.addView(textView);
                card.getCells()[row][column].setTextViewOfACell(textView);

                child.setOnClickListener(cellTap(row, column, image, textView));

                parent.addView(child);
                column++;
            }
            grid.addView(parent);
            row++;
            column = 0;
        }
    }

    private View.OnClickListener cellTap(final int row, final int column, final ImageView image, final TextView textView) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int number = card.getCells()[row][column].getNumber();
                if (!card.getCells()[row][column].isDaubed() && draw.getDrawnNumbers().get(number)) {
                    GameMusic.getInstance().playSound(textView.getContext(), GameMusic.TAP);
                    daubCell(image, card, row, column, textView);
                } else if (!draw.getDrawnNumbers().get(number) && !card.getCells()[row][column].isDaubed()) {
                    fight.missClick();
                    testOfDeath();
                }
            }
        };
    }

    public void daubCell(final ImageView image, Card card, int row, int column, final TextView textView) {
        if (monster.getHealth()[0] < 1 || player.getHealth()[0] < 1) {
            return;
        }
        animate(image, textView);
        ((RelativeLayout) image.getParent()).setBackgroundResource(R.drawable.border_gray);
        card.getCells()[row][column].setDaubed(true);
        textView.setTextColor(Color.WHITE);

        contentHandler.useContent(card.getCells()[row][column].getContent());
        contentHandler.useColor(card.getCells()[row][column].getColor());
        fight.monsterIsAttacked();
        testOfDeath();
    }

    private void animate(final ImageView image, final TextView textView) {
        final Drawable drawable = textView.getBackground();
        AnimationNew animation = new AnimationNew((AnimationDrawable) getResources().getDrawable(R.drawable.animation_ring)) {
            @Override
            public void onAnimationFinish() {
                image.setImageResource(R.drawable.daub);
                image.setAlpha(1f);
                textView.setBackground(drawable);
            }
        };
        textView.setBackground(animation);
        animation.start();
    }

    public void drawNumber() {
        int number = draw.drawNumber();
        numbersTextViews.get(number).setTextColor(Color.GREEN);

        for (int i = lastDraws.size() - 1; i > 0; i--) {
            String m = lastDraws.get(i - 1).getText().toString();
            lastDraws.get(i).setText(m);
        }
        lastDraws.get(0).setText(String.valueOf(number));

        GameMusic.getInstance().playSound(this, GameMusic.DRAW);
    }

    public void check(View view) {
        int numberOfBingo = card.checkForBingo();
        if (numberOfBingo > 0) {
            int damage = fight.bingoDamage(numberOfBingo);
            handler.removeCallbacksAndMessages(null);
            if (monster.getHealth()[0] <= 0 && !popUpInitiated) {
                testOfDeath();
            } else {
                createNewCard();
            }
            String numberOfBingoMessage = String.format(Locale.getDefault(), getString(R.string.numberOfBingo), numberOfBingo);
            String damageMessage = String.format(Locale.getDefault(), getString(R.string.playerDealDamage), damage);
            updateMessages(String.format("%s %s", numberOfBingoMessage, damageMessage));
        } else {
            fight.bingoMiss();
            testOfDeath();
        }
    }

    private void showPopup(boolean monsterDied) {
        Button button = ((Button) getLayoutInflater().inflate(R.layout.button, null));
        button.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        button.setText(getString(R.string.accept));
        PopupWindow window = GameActivityViews.createPopUp(this, handler, button, margin, grid, monsterDied);
        button.setOnClickListener(setListenerForPopup(window));
    }

    private View.OnClickListener setListenerForPopup(final PopupWindow window) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                window.dismiss();
                if (player.getHealth()[0] <= 0) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
                    pref.edit().clear().apply();
                    handler.removeCallbacksAndMessages(null);
                    handler = null;
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent, AnimationNew.activityTransition(getApplicationContext()).toBundle());
                    finish();
                } else {
                    goToSummary();
                }
                popUpInitiated = false;
            }
        };
    }

    private void goToSummary() {
        updateMessages(getString(R.string.monsterKilled));
        Intent intent = new Intent(this, Summary.class);
        intent.putExtra(Summary.EXPERIENCE_REWARD, monster.getExperienceReward());
        intent.putExtra(Summary.GOLD_REWARD, monster.getGoldReward());
        intent.putExtra(Summary.ITEM_REWARD, monster.getItemReward());
        updateAfterFight();
        int levels = levelUp();
        intent.putExtra(Summary.LEVEL_UP, levels);
        intent.putExtra(Summary.PLAYER, player);
        startActivity(intent, AnimationNew.activityTransition(this).toBundle());
    }

    private int levelUp() {
        int newLevels = 0;
        while (player.getExperience()[0] >= player.getExperience()[1]) {
            player.getExperience()[0] -= player.getExperience()[1];
            player.setLevel(player.getLevel() + 1);
            player.getExperience()[1] = player.toNextLevel();
            newLevels++;
        }
        return newLevels;
    }

    private void updateAfterFight() {
        player.setGold(player.getGold() + monster.getGoldReward());
        player.getExperience()[0] += monster.getExperienceReward();
        player.setMonsterLevel(player.getMonsterLevel() + 1);
        monster.createMonster(player.getMonsterLevel(), this);
    }

    public void updateMessages(String message) {
        messageLog.append("\n" + message);
        log.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                log.post(new Runnable() {
                    public void run() {
                        log.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        });
    }

    private void setArrays() {
        numbersTextViews = new ArrayList<>();
        lastDraws = new ArrayList<>();

        lastDraws.add(lastDraw);
        lastDraws.add(secondLast);
        lastDraws.add(thirdLast);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchNumbersAndLog();
            }
        };
        log.setOnClickListener(listener);
        messageLog.setOnClickListener(listener);
        drawnNumbers.setOnClickListener(listener);
    }

    private void setSkillButtons() {
        if (!player.getSkills().get(Skill.Name.DEFENDER)) {
            disableView(useDefender);
        } else {
            enableView(useDefender);
        }
        if (!player.getSkills().get(Skill.Name.FREE_TAP)) {
            disableView(useFreeTap);
        } else {
            enableView(useFreeTap);
        }
        if (!player.getSkills().get(Skill.Name.GOLD_RAIN)) {
            disableView(useGoldRain);
        } else {
            enableView(useGoldRain);
        }
        if (!player.getSkills().get(Skill.Name.HEAL)) {
            disableView(useHeal);
        } else {
            enableView(useHeal);
        }
        if (!player.getSkills().get(Skill.Name.MANA_RAIN)) {
            disableView(useMagicChannel);
        } else {
            enableView(useMagicChannel);
        }
        if (!player.getSkills().get(Skill.Name.PATIENCE)) {
            disableView(usePatience);
        } else {
            enableView(usePatience);
        }
        if (!player.getSkills().get(Skill.Name.RAGE)) {
            disableView(useRage);
        } else {
            enableView(useRage);
        }
        if (!player.getSkills().get(Skill.Name.SCAVENGE)) {
            disableView(useScavenger);
        } else {
            enableView(useScavenger);
        }
        if (!player.getSkills().get(Skill.Name.WISDOM)) {
            disableView(useWisdom);
        } else {
            enableView(useWisdom);
        }
        if (player.getHealingPotion().getAmount() < 1) {
            disableView(useHealingPotion);
        } else {
            enableView(useHealingPotion);
        }
        if (player.getMagicPotion().getAmount() < 1) {
            disableView(useMagicPotion);
        } else {
            enableView(useMagicPotion);
        }
        if (player.getBomb().getAmount() < 1) {
            disableView(useBomb);
        } else {
            enableView(useBomb);
        }

        costDefender.setText(String.valueOf(Skill.DEFENDER_COST));
        costFreeTap.setText(String.valueOf(Skill.FREE_TAP_COST));
        costGoldRain.setText(String.valueOf(Skill.GOLD_RAIN_COST));
        costHeal.setText(String.valueOf(Skill.HEAL_COST));
        costMagicChannel.setText(String.valueOf(Skill.MAGIC_CHANNEL_COST));
        costPatience.setText(String.valueOf(Skill.PATIENCE_COST));
        costRage.setText(String.valueOf(Skill.RAGE_COST));
        costScavenger.setText(String.valueOf(Skill.SCAVENGER_COST));
        costWisdom.setText(String.valueOf(Skill.WISDOM_COST));
        amountHealPotions.setText(String.valueOf(player.getHealingPotion().getAmount()));
        amountMagicPotions.setText(String.valueOf(player.getMagicPotion().getAmount()));
        amountBombs.setText(String.valueOf(player.getBomb().getAmount()));
    }

    private void disableView(View view) {
        view.setAlpha(0.15f);
        view.setEnabled(false);
    }

    private void enableView(View view) {
        view.setAlpha(1f);
        view.setEnabled(true);
    }

    private void initiateInfoInView() {
        int agility = player.getTotalAgility();
        int endurance = player.getTotalEndurance();
        int magic = player.getTotalMagic();
        int luck = player.getTotalLuck();
        int strength = player.getTotalStrength();

        playerLevel.setText(String.valueOf(player.getLevel()));
        plaAgi.setText(String.valueOf(agility));
        plaStr.setText(String.valueOf(strength));
        plaEnd.setText(String.valueOf(endurance));
        plaMag.setText(String.valueOf(magic));
        plaLck.setText(String.valueOf(luck));
        gold.setText(String.valueOf(player.getGold()));
        experience.setText(String.valueOf(player.getExperience()[0]));
        experienceGoal.setText(String.valueOf(player.getExperience()[1]));
        plaHp.setText(String.valueOf(player.getHealth()[0]));
        plaHpMax.setText(String.valueOf(player.getHealth()[1]));
        plaMp.setText(String.valueOf(player.getMagicPower()[0]));
        plaMpMax.setText(String.valueOf(player.getMagicPower()[1]));
        plaDmgMin.setText(String.valueOf(player.getWeapon().getDiceQuantity()));
        plaDmgMax.setText(String.valueOf(player.getWeapon().getDiceQuantity() * player.getWeapon().getDiceStrength()));

        monAgi.setText(String.valueOf(monster.getAgility()));
        monEnd.setText(String.valueOf(monster.getEndurance()));
        monStr.setText(String.valueOf(monster.getStrength()));
        monMag.setText(String.valueOf(monster.getMagic()));
        minDamageMonster.setText(String.valueOf(monster.getDiceNumber()));
        maxDamageMonster.setText(String.valueOf(monster.getDiceNumber() * monster.getDiceStrength()));
        monHp.setText(String.valueOf(monster.getHealth()[0]));
        monHpMax.setText(String.valueOf(monster.getHealth()[1]));
        monsterLevel.setText(String.valueOf(player.getMonsterLevel()));
        monsterImage.setImageResource(GameResources.monsterImage(monster.getType()));
        monsterName.setText(monster.getName());
    }

    public void pauseDraw() {
        pause = !pause;
        if (pause) {
            LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            viewParams.setMargins(margin, margin, margin, margin);

            TextView text = TextViewBuilder.createText(this, getString(R.string.gamePaused), viewParams, Gravity.CENTER, Color.WHITE, Typeface.BOLD_ITALIC);
            text.setBackgroundColor(Color.BLACK);
            final PopupWindow window = new PopupWindow(text, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT, true);
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    window.dismiss();
                    pauseDraw();
                }
            });
            window.showAtLocation(grid, Gravity.CENTER, 0, 0);

            counterStopTime = System.currentTimeMillis();
            handler.removeCallbacksAndMessages(null);
        } else {
            setDrawingMachine(drawInterval, drawInterval - (counterStopTime - counterStartTime));
        }
    }

    private void switchNumbersAndLog() {
        int logVisibility = log.getVisibility();
        if (logVisibility == View.GONE) {
            log.setVisibility(View.VISIBLE);
            messageLog.setVisibility(View.VISIBLE);
            drawnNumbers.setVisibility(View.GONE);
        } else {
            drawnNumbers.setVisibility(View.VISIBLE);
            log.setVisibility(View.GONE);
            messageLog.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.pause)
    public void pauseButton() {
        pauseDraw();
    }

    @OnClick({R.id.useDefender, R.id.useFreeTap, R.id.useGoldRain, R.id.useHeal, R.id.useMagicChannel, R.id.usePatience,
            R.id.useRage, R.id.useScavenger, R.id.useWisdom, R.id.useHealingPotion, R.id.useMagicPotion, R.id.useBomb,})
    public void useSkillOrItem(View view) {
        switch (view.getId()) {
            case R.id.useBomb:
                player.getBomb().use(player, monster, this);
                amountBombs.setText(String.valueOf(player.getBomb().getAmount()));
                monHp.setText(String.valueOf(monster.getHealth()[0]));
                if (player.getBomb().getAmount() == 0) {
                    disableView(view);
                }
                testOfDeath();
                break;
            case R.id.useHealingPotion:
                player.getHealingPotion().use(player, monster, this);
                amountHealPotions.setText(String.valueOf(player.getHealingPotion().getAmount()));
                plaHp.setText(String.valueOf(player.getHealth()[0]));
                if (player.getHealingPotion().getAmount() == 0) {
                    disableView(view);
                }
                break;
            case R.id.useMagicPotion:
                player.getMagicPotion().use(player, monster, this);
                amountMagicPotions.setText(String.valueOf(player.getMagicPotion().getAmount()));
                plaMp.setText(String.valueOf(player.getMagicPower()[0]));
                if (player.getMagicPotion().getAmount() == 0) {
                    disableView(view);
                }
                break;
            default:
                skill.use(view.getId());
                break;
        }
    }

    private void testOfDeath() {
        if (fight.someoneIsDead() && !popUpInitiated) {
            popUpInitiated = true;
            if (player.getHealth()[0] <= 0) {
                showPopup(false);
                GameMusic.getInstance().playSound(this, GameMusic.DEATH);
            } else {
                fightIsOver = true;
                GameMusic.getInstance().playSound(this, GameMusic.FIGHT_WON);
                showPopup(true);
            }
        }
    }

    public TextView getExperience() {
        return experience;
    }

    public TextView getGold() {
        return gold;
    }

    public TextView getPlaHp() {
        return plaHp;
    }

    public TextView getMonHp() {
        return monHp;
    }

    public TextView getPlaMp() {
        return plaMp;
    }

}