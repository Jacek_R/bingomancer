package darkspace.bingomancer.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import darkspace.bingomancer.R;
import darkspace.bingomancer.builders.GameOverDialog;
import darkspace.bingomancer.builders.ToastBuilder;
import darkspace.bingomancer.data.Const;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.gameplay.Skill;
import darkspace.bingomancer.utils.GameMusic;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LevelUp extends AppCompatActivity {

    private int statPointsRemaining;
    private int skillPoints;

    private int strength;
    private int endurance;
    private int luck;
    private int magic;
    private int agility;

    private int investedSkillPoints;

    private boolean defender;
    private boolean freeTap;
    private boolean goldRain;
    private boolean heal;
    private boolean magicChannel;
    private boolean patience;
    private boolean rage;
    private boolean scavenger;
    private boolean wisdom;

    private Player player;

    @BindView(R.id.levelUpAgiValue)
    TextView valueAgility;
    @BindView(R.id.levelUpEndValue)
    TextView valueEndurance;
    @BindView(R.id.levelUpStrValue)
    TextView valueStrength;
    @BindView(R.id.levelUpMagValue)
    TextView valueMagic;
    @BindView(R.id.levelUpLckValue)
    TextView valueLuck;
    @BindView(R.id.levelUpStatPoints)
    TextView statsRemainValue;
    @BindView(R.id.levelUpSkillPoints)
    TextView skillRemainValue;

    @BindView(R.id.lvlUpDefender)
    FrameLayout skillDefender;
    @BindView(R.id.lvlUpFreeTap)
    FrameLayout skillFreeTap;
    @BindView(R.id.lvlUpGoldRain)
    FrameLayout skillGoldRain;
    @BindView(R.id.lvlUpHeal)
    FrameLayout skillHeal;
    @BindView(R.id.lvlUpMagicChannel)
    FrameLayout skillMagicChannel;
    @BindView(R.id.lvlUpPatience)
    FrameLayout skillPatience;
    @BindView(R.id.lvlUpRage)
    FrameLayout skillRage;
    @BindView(R.id.lvlUpScavenger)
    FrameLayout skillScavenger;
    @BindView(R.id.lvlUpWisdom)
    FrameLayout skillWisdom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_up);
        ButterKnife.bind(this);

        player = ((Player) getIntent().getSerializableExtra(Summary.PLAYER));
        initialValues();
        GameMusic.getInstance().playSound(this, GameMusic.LEVEL_UP);
        GameMusic.activities_open++;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameMusic.getInstance().continuePlaying();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing()) {
            GameMusic.getInstance().pauseMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GameMusic.activities_open--;
        GameMusic.getInstance().destroyMusic();
    }

    @Override
    public void onBackPressed() {
        new GameOverDialog().show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.popup_show, R.anim.popup_hide);
    }

    private void initialValues() {
        investedSkillPoints = 0;
        statPointsRemaining = 0;
        skillPoints = 0;

        defender = player.getSkills().get(Skill.Name.DEFENDER);
        freeTap = player.getSkills().get(Skill.Name.FREE_TAP);
        goldRain = player.getSkills().get(Skill.Name.GOLD_RAIN);
        heal = player.getSkills().get(Skill.Name.HEAL);
        magicChannel = player.getSkills().get(Skill.Name.MANA_RAIN);
        patience = player.getSkills().get(Skill.Name.PATIENCE);
        rage = player.getSkills().get(Skill.Name.RAGE);
        scavenger = player.getSkills().get(Skill.Name.SCAVENGE);
        wisdom = player.getSkills().get(Skill.Name.WISDOM);

        checkSkill(defender, skillDefender);
        checkSkill(freeTap, skillFreeTap);
        checkSkill(goldRain, skillGoldRain);
        checkSkill(heal, skillHeal);
        checkSkill(magicChannel, skillMagicChannel);
        checkSkill(patience, skillPatience);
        checkSkill(rage, skillRage);
        checkSkill(scavenger, skillScavenger);
        checkSkill(wisdom, skillWisdom);

        int newLevels = getIntent().getIntExtra(Summary.LEVEL_UP, 0);
        while (newLevels > 0) {
            if (investedSkillPoints == Const.NUMBER_OF_SKILLS) {
                statPointsRemaining += 8;
            } else {
                investedSkillPoints++;
                statPointsRemaining += 5;
                skillPoints++;
            }
            newLevels--;
        }

        strength = player.getStrength();
        endurance = player.getEndurance();
        luck = player.getLuck();
        magic = player.getMagic();
        agility = player.getAgility();

        valueAgility.setText(String.valueOf(agility));
        valueEndurance.setText(String.valueOf(endurance));
        valueMagic.setText(String.valueOf(magic));
        valueStrength.setText(String.valueOf(strength));
        valueLuck.setText(String.valueOf(luck));

        skillRemainValue.setText(String.valueOf(skillPoints));
        statsRemainValue.setText(String.valueOf(statPointsRemaining));
    }

    private void checkSkill(boolean valueOfSkill, FrameLayout layout) {
        if (valueOfSkill) {
            layout.setAlpha(0.35f);
            layout.setEnabled(false);
            investedSkillPoints++;
        } else {
            layout.setAlpha(1f);
            layout.setEnabled(true);
        }
    }

    @OnClick({R.id.lvlUpDefender, R.id.lvlUpFreeTap, R.id.lvlUpGoldRain, R.id.lvlUpHeal,
            R.id.lvlUpMagicChannel, R.id.lvlUpPatience, R.id.lvlUpRage, R.id.lvlUpScavenger, R.id.lvlUpWisdom})
    public void pickSkill(FrameLayout view) {
        if (skillPoints < 1) {
            return;
        }
        switch (view.getId()) {
            case R.id.lvlUpDefender:
                defender = true;
                break;
            case R.id.lvlUpFreeTap:
                freeTap = true;
                break;
            case R.id.lvlUpGoldRain:
                goldRain = true;
                break;
            case R.id.lvlUpHeal:
                heal = true;
                break;
            case R.id.lvlUpMagicChannel:
                magicChannel = true;
                break;
            case R.id.lvlUpPatience:
                patience = true;
                break;
            case R.id.lvlUpRage:
                rage = true;
                break;
            case R.id.lvlUpScavenger:
                scavenger = true;
                break;
            case R.id.lvlUpWisdom:
                wisdom = true;
                break;
        }
        skillPoints--;
        checkSkill(true, view);
        skillRemainValue.setText(String.valueOf(skillPoints));
    }

    @OnClick({R.id.levelUpAgiPlus, R.id.levelUpEndPlus, R.id.levelUpLckPlus, R.id.levelUpStrPlus, R.id.levelUpMagPlus})
    public void modifyStat(Button view) {
        switch (view.getId()) {
            case R.id.levelUpAgiPlus:
                agility += plusStat(valueAgility, agility);
                break;
            case R.id.levelUpEndPlus:
                endurance += plusStat(valueEndurance, endurance);
                break;
            case R.id.levelUpStrPlus:
                strength += plusStat(valueStrength, strength);
                break;
            case R.id.levelUpLckPlus:
                luck += plusStat(valueLuck, luck);
                break;
            case R.id.levelUpMagPlus:
                magic += plusStat(valueMagic, magic);
                break;
        }
    }

    private int plusStat(TextView value, int currentStatValue) {
        if (statPointsRemaining == 0) {
            return 0;
        } else {
            statPointsRemaining--;
            value.setText(String.valueOf(currentStatValue + 1));
            statsRemainValue.setText(String.valueOf(statPointsRemaining));
            return 1;
        }
    }

    public void accept(View view) {
        if (statPointsRemaining > 0 || skillPoints > 0) {
            ToastBuilder.create(getString(R.string.pointsLeft), this).show();
            return;
        }
        player.setStrength(strength);
        player.setAgility(agility);
        player.setMagic(magic);
        player.setEndurance(endurance);
        player.setLuck(luck);

        player.getSkills().put(Skill.Name.DEFENDER, defender);
        player.getSkills().put(Skill.Name.FREE_TAP, freeTap);
        player.getSkills().put(Skill.Name.GOLD_RAIN, goldRain);
        player.getSkills().put(Skill.Name.HEAL, heal);
        player.getSkills().put(Skill.Name.MANA_RAIN, magicChannel);
        player.getSkills().put(Skill.Name.PATIENCE, patience);
        player.getSkills().put(Skill.Name.RAGE, rage);
        player.getSkills().put(Skill.Name.SCAVENGE, scavenger);
        player.getSkills().put(Skill.Name.WISDOM, wisdom);

        int newMaxMp = player.maximumMagicPower();
        int newMaxHp = player.maximumHealth();

        player.getMagicPower()[0] += newMaxMp - player.getMagicPower()[1];
        player.getHealth()[0] += newMaxHp - player.getHealth()[1];

        player.getMagicPower()[1] = newMaxMp;
        player.getHealth()[1] = newMaxHp;

        EventBus.getDefault().post(player);
        finish();
    }

    public void reset(View view) {
        initialValues();
    }
}
