package darkspace.bingomancer.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import darkspace.bingomancer.R;
import darkspace.bingomancer.builders.GameOverDialog;
import darkspace.bingomancer.builders.ImageViewBuilder;
import darkspace.bingomancer.builders.LinearLayoutBuilder;
import darkspace.bingomancer.builders.TextViewBuilder;
import darkspace.bingomancer.data.GameResources;
import darkspace.bingomancer.data.Player;
import darkspace.bingomancer.items.Armor;
import darkspace.bingomancer.items.Item;
import darkspace.bingomancer.items.Shield;
import darkspace.bingomancer.items.Weapon;
import darkspace.bingomancer.utils.AnimationNew;
import darkspace.bingomancer.utils.GameMusic;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CollectItem extends AppCompatActivity {

    @BindView(R.id.acceptItem)
    Button accept;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.headerItem)
    TextView header;
    @BindView(R.id.switchItem)
    Button switchItem;
    @BindView(R.id.discardItem)
    Button discardItem;
    @BindDimen(R.dimen.largeMargin)
    int largeMargin;
    @BindDimen(R.dimen.mediumMargin)
    int mediumMargin;
    @BindDimen(R.dimen.smallMargin)
    int smallMargin;

    private int newLevels;
    private int items;
    private Player player;
    private Animation animation;

    private final static int WEAPON = 1;
    private final static int ARMOR = 2;
    private final static int SHIELD = 3;
    private final static int BOMB = 4;
    private final static int HEALING_POTION = 5;
    private final static int MAGIC_POTION = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collect_item);
        ButterKnife.bind(this);
        items = getIntent().getIntExtra(Summary.ITEM_REWARD, 0);
        newLevels = getIntent().getIntExtra(Summary.LEVEL_UP, 0);
        player = ((Player) getIntent().getSerializableExtra(Summary.PLAYER));
        accept.setEnabled(false);

        animation = new ScaleAnimation(0, 1f, 0, 1f);
        animation.setDuration(1000);
        container.setAnimation(animation);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                receiveAnItem();
            }
        }, 500);
        GameMusic.activities_open++;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameMusic.getInstance().continuePlaying();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing()) {
            GameMusic.getInstance().pauseMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GameMusic.activities_open--;
        GameMusic.getInstance().destroyMusic();
    }

    @Override
    public void onBackPressed() {
        new GameOverDialog().show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.popup_show, R.anim.popup_hide);
    }

    private void receiveAnItem() {
        Random rng = new Random();
        int seed = rng.nextInt(100);
        if (seed < 30) {
            switchItem.setEnabled(false);
            discardItem.setEnabled(false);
            seed = rng.nextInt(99);
            boolean additionalItem = rng.nextInt(100) < player.getTotalLuck();
            int amount;
            if (additionalItem) {
                amount = 2;
            } else {
                amount = 1;
            }
            if (seed < 33) {
                receivedConsumable(BOMB, amount);
                amount += player.getBomb().getAmount();
                player.getBomb().setAmount(amount);
            } else if (seed < 66) {
                receivedConsumable(HEALING_POTION, amount);
                amount += player.getHealingPotion().getAmount();
                player.getHealingPotion().setAmount(amount);
            } else {
                receivedConsumable(MAGIC_POTION, amount);
                amount += player.getMagicPotion().getAmount();
                player.getMagicPotion().setAmount(amount);
            }
        } else {
            seed = rng.nextInt(99);
            if (seed < 33) {
                receivedEquippable(WEAPON);
            } else if (seed < 66) {
                receivedEquippable(ARMOR);
            } else {
                receivedEquippable(SHIELD);
            }
        }
        container.startAnimation(animation);
        if (header.getVisibility() == View.INVISIBLE) {
            header.setVisibility(View.VISIBLE);
        }
    }

    private void receivedConsumable(int itemId, int amount) {
        container.removeAllViews();
        String message;
        String plural = getString(R.string.plural);
        String singular = getString(R.string.singular);
        int drawable;
        switch (itemId) {
            case BOMB:
                message = String.format(Locale.getDefault(), getString(R.string.scrollReceived), amount);
                drawable = R.drawable.bomb;
                break;
            case MAGIC_POTION:
                message = String.format(Locale.getDefault(), getString(R.string.magicPotionReceived), amount);
                drawable = R.drawable.magic_potion;
                break;
            case HEALING_POTION:
                message = String.format(Locale.getDefault(), getString(R.string.healingPotionReceived), amount);
                drawable = R.drawable.healing_potion;
                break;
            default:
                message = "";
                drawable = 0;
                break;
        }
        if (amount == 1) {
            message += singular;
        } else {
            message += plural;
        }

        header.setText(message);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout layout = LinearLayoutBuilder.createLinearLayout(this, params, 10, LinearLayout.HORIZONTAL);
        ImageView imageView = ImageViewBuilder.createImage(this, drawable);
        imageView.setBackgroundResource(R.drawable.empty_frame_normal);
        layout.addView(imageView);
        container.addView(layout);
        accept.setEnabled(true);
    }

    private void receivedEquippable(int itemId) {
        container.removeAllViews();
        String message;
        Item item;
        Item newItem;
        switch (itemId) {
            case WEAPON:
                item = player.getWeapon();
                newItem = new Weapon(player.getTotalLuck(), player.getMonsterLevel());
                message = getString(R.string.weaponReceived);
                break;
            case ARMOR:
                item = player.getArmor();
                newItem = new Armor(player.getTotalLuck(), player.getMonsterLevel());
                message = getString(R.string.armorReceived);
                break;
            case SHIELD:
                item = player.getShield();
                newItem = new Shield(player.getTotalLuck(), player.getMonsterLevel());
                message = getString(R.string.shieldReceived);
                break;
            default:
                message = "";
                item = new Item();
                newItem = new Item();
                break;
        }
        String quality;
        switch (newItem.getQuality()) {
            case NORMAL:
                quality = getString(R.string.qualityNormal);
                break;
            case MAGIC:
                quality = getString(R.string.qualityMagic);
                break;
            case LEGENDARY:
                quality = getString(R.string.qualityLegendary);
                break;
            default:
                quality = "";
                break;
        }
        message = String.format(Locale.getDefault(), message, quality);
        message += " ";
        message += getString(R.string.switchItemPrompt);

        header.setText(message);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout mainLayout = LinearLayoutBuilder.createLinearLayout(this, params, 10, LinearLayout.HORIZONTAL);

        mainLayout.addView(createLayoutWithItem(
                item.getQuality(), GameResources.itemToDrawable(item), item.getStrength(), item.getEndurance(), item.getAgility(), item.getLuck(),
                item.getMagic(), item.getDodge(), item.getBlock(), item.getDiceStrength(), item.getDiceQuantity(), getString(R.string.oldItem)));

        mainLayout.addView(createLayoutWithItem(
                newItem.getQuality(), GameResources.itemToDrawable(newItem), newItem.getStrength(), newItem.getEndurance(), newItem.getAgility(), newItem.getLuck(),
                newItem.getMagic(), newItem.getDodge(), newItem.getBlock(), newItem.getDiceStrength(), newItem.getDiceQuantity(), getString(R.string.newItem)));
        switchItem.setOnClickListener(switchItem(newItem, discardItem, item));
        discardItem.setOnClickListener(discardItem(switchItem, newItem));
        container.addView(mainLayout);
    }

    private LinearLayout createLayoutWithItem(
            Item.Quality quality, int drawable, int str, int end, int agi, int lck, int mag, int dodge, int block, int diceStr, int diceQua, String message) {

        int damageMax = diceQua * diceStr;
        int damage = Math.round(diceQua + damageMax);
        int sellValue = (str + end + agi + lck + mag) / 2;
        ArrayList<Integer> statValues = new ArrayList<>();
        ArrayList<String> messages = new ArrayList<>();

        statValues.add(agi);
        statValues.add(end);
        statValues.add(mag);
        statValues.add(lck);
        statValues.add(str);
        statValues.add(sellValue);

        statValues.add(block);
        statValues.add(damage);
        statValues.add(dodge);

        messages.add(getString(R.string.blockShort));
        messages.add(getString(R.string.damageShort));
        messages.add(getString(R.string.dodgeShort));

        LinearLayout.LayoutParams mainLayoutParams =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mainLayoutParams.setMargins(mediumMargin, mediumMargin, mediumMargin, mediumMargin);
        LinearLayout mainLayout =
                LinearLayoutBuilder.createLinearLayout(this, mainLayoutParams, LinearLayout.VERTICAL);
        mainLayout.setBackgroundResource(R.drawable.gui_window_vertical);

        LinearLayout.LayoutParams headerParams =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        headerParams.setMargins(largeMargin, largeMargin, largeMargin, smallMargin);
        TextView header = TextViewBuilder.createText(
                this, message, headerParams, Gravity.CENTER, Color.BLACK, Typeface.BOLD);

        LinearLayout.LayoutParams imageParams =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageParams.setMargins(smallMargin, smallMargin, smallMargin, smallMargin);
        imageParams.gravity = Gravity.CENTER;

        ImageView image = ImageViewBuilder.createImage(this, drawable);
        image.setLayoutParams(imageParams);
        switch (quality) {
            case NORMAL:
                image.setBackgroundResource(R.drawable.empty_frame_normal);
                break;
            case MAGIC:
                image.setBackgroundResource(R.drawable.empty_frame_magic);
                break;
            case LEGENDARY:
                image.setBackgroundResource(R.drawable.empty_frame_legendary);
                break;
        }
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        LinearLayout.LayoutParams textViewParams =
                new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        textViewParams.setMargins(largeMargin, 0, mediumMargin, 0);

        LinearLayout.LayoutParams containersParams =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        LinearLayout firstContainer =
                LinearLayoutBuilder.createLinearLayout(this, containersParams, LinearLayout.HORIZONTAL);
        firstContainer.addView(TextViewBuilder.createText(
                this, getString(R.string.agilityShort), textViewParams, Gravity.START, Color.BLACK, Typeface.BOLD));
        firstContainer.addView(TextViewBuilder.createText(
                this, String.valueOf(statValues.get(0)), textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));
        firstContainer.addView(TextViewBuilder.createText(
                this, getString(R.string.enduranceShort), textViewParams, Gravity.START, Color.BLACK, Typeface.BOLD));
        firstContainer.addView(TextViewBuilder.createText(
                this, String.valueOf(statValues.get(1)), textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));

        LinearLayout secondContainer =
                LinearLayoutBuilder.createLinearLayout(this, containersParams, LinearLayout.HORIZONTAL);
        secondContainer.addView(TextViewBuilder.createText(
                this, getString(R.string.magicShort), textViewParams, Gravity.START, Color.BLACK, Typeface.BOLD));
        secondContainer.addView(TextViewBuilder.createText(
                this, String.valueOf(statValues.get(2)), textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));
        secondContainer.addView(TextViewBuilder.createText(
                this, getString(R.string.luckShort), textViewParams, Gravity.START, Color.BLACK, Typeface.BOLD));
        secondContainer.addView(TextViewBuilder.createText(
                this, String.valueOf(statValues.get(3)), textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));

        LinearLayout thirdContainer =
                LinearLayoutBuilder.createLinearLayout(this, containersParams, LinearLayout.HORIZONTAL);
        thirdContainer.addView(TextViewBuilder.createText(
                this, getString(R.string.strengthShort), textViewParams, Gravity.START, Color.BLACK, Typeface.BOLD));
        thirdContainer.addView(TextViewBuilder.createText(
                this, String.valueOf(statValues.get(4)), textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));
        thirdContainer.addView(TextViewBuilder.createText(
                this, getString(R.string.valueShort), textViewParams, Gravity.START, Color.BLACK, Typeface.BOLD));
        thirdContainer.addView(TextViewBuilder.createText(
                this, String.valueOf(statValues.get(5)), textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));

        LinearLayout fourthContainer =
                LinearLayoutBuilder.createLinearLayout(this, containersParams, LinearLayout.HORIZONTAL);

        for (int i = 6; i < 9; i++) {
            if (statValues.get(i) > 0) {
                fourthContainer.addView(TextViewBuilder.createText(
                        this, messages.get(i - 6), textViewParams, Gravity.START, Color.BLACK, Typeface.BOLD));
                if (i == 7) {
                    String damageMessage = String.format(Locale.getDefault(), getString(R.string.damageRange), diceQua, damageMax);
                    fourthContainer.addView(TextViewBuilder.createText(
                            this, damageMessage, textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));
                } else {
                    fourthContainer.addView(TextViewBuilder.createText(
                            this, String.valueOf(statValues.get(i)), textViewParams, Gravity.START, Color.BLACK, Typeface.NORMAL));
                }
                break;
            }
        }

        mainLayout.addView(header);
        mainLayout.addView(image);
        mainLayout.addView(firstContainer);
        mainLayout.addView(secondContainer);
        mainLayout.addView(thirdContainer);
        mainLayout.addView(fourthContainer);

        return mainLayout;
    }

    private View.OnClickListener switchItem(final Item newItem, final Button discardButton, final Item item) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int sellValue = (
                        item.getAgility() + item.getEndurance() + item.getLuck() + item.getStrength() + item.getMagic()) / 2;

                if (newItem instanceof Weapon) {
                    player.getWeapon().updateItem(newItem.getStrength(), newItem.getEndurance(), newItem.getAgility(),
                            newItem.getMagic(), newItem.getLuck(), newItem.getDiceStrength(), newItem.getDiceQuantity(), newItem.getQuality());
                } else if (newItem instanceof Armor) {
                    player.getArmor().updateItem(newItem.getStrength(), newItem.getEndurance(), newItem.getAgility(),
                            newItem.getMagic(), newItem.getLuck(), newItem.getDodge(), newItem.getQuality());
                } else {
                    player.getShield().updateItem(newItem.getStrength(), newItem.getEndurance(), newItem.getAgility(),
                            newItem.getMagic(), newItem.getLuck(), newItem.getBlock(), newItem.getQuality());
                }
                view.setEnabled(false);
                discardButton.setEnabled(false);
                player.setGold(player.getGold() + sellValue);
                accept.setEnabled(true);
            }
        };
    }

    private View.OnClickListener discardItem(final Button switchButton, final Item item) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setEnabled(false);
                switchButton.setEnabled(false);
                int sellValue = (
                        item.getAgility() + item.getMagic() + item.getStrength() + item.getEndurance() + item.getLuck()) / 2;
                player.setGold(player.getGold() + sellValue);
                GameMusic.getInstance().playSound(view.getContext(), GameMusic.SELL);
                accept.setEnabled(true);
            }
        };
    }

    public void acceptItem(View view) {
        items--;
        if (items > 0) {
            accept.setEnabled(false);
            switchItem.setEnabled(true);
            discardItem.setEnabled(true);
            receiveAnItem();
        } else if (newLevels > 0) {
            Intent intent = new Intent(this, LevelUp.class);
            intent.putExtra(Summary.PLAYER, player);
            intent.putExtra(Summary.LEVEL_UP, newLevels);
            startActivity(intent, AnimationNew.activityTransition(this).toBundle());
            finish();
        } else {
            EventBus.getDefault().post(player);
            finish();
        }
    }

}
