package darkspace.bingomancer.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import darkspace.bingomancer.R;
import darkspace.bingomancer.builders.GameOverDialog;
import darkspace.bingomancer.utils.AnimationNew;
import darkspace.bingomancer.utils.GameMusic;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    public final static String LOAD_GAME = "loadGame";
    @BindView(R.id.continueGame)
    Button continueGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ButterKnife.bind(this);
        checkIfSaveDataSExists();
        GameMusic.getInstance().initiateMusicPreferences(this);
        if (GameMusic.getInstance().getMusic() == null) {
            GameMusic.getInstance().loadTrack(this, GameMusic.MENU);
        }
        GameMusic.activities_open++;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameMusic.getInstance().continuePlaying();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing()) {
            GameMusic.getInstance().pauseMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GameMusic.activities_open--;
        GameMusic.getInstance().destroyMusic();
    }

    @Override
    public void onBackPressed() {
        new GameOverDialog().show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void checkIfSaveDataSExists() {
        SharedPreferences pref = this.getSharedPreferences(Game.PREFERENCES_KEY, Context.MODE_PRIVATE);
        if (!pref.contains(Game.PLAYER_KEY)) {
            continueGame.setEnabled(false);
        }
    }

    @OnClick(R.id.newGame)
    public void newGame() {
        Intent intent = new Intent(this, NewGame.class);
        startActivity(intent, AnimationNew.activityTransition(this).toBundle());
        finish();
    }

    @OnClick(R.id.continueGame)
    public void continueGame() {
        Intent intent = new Intent(this, Game.class);
        intent.putExtra(LOAD_GAME, true);
        startActivity(intent, AnimationNew.activityTransition(this).toBundle());
        finish();
    }

    @OnClick(R.id.options)
    public void options() {
        Intent intent = new Intent(this, HelpOptions.class);
        startActivity(intent, AnimationNew.activityTransition(this).toBundle());
        finish();
    }

    @OnClick(R.id.exit)
    public void exit() {
        finish();
    }
}
