Gra na urządzenia z systemem Android będąca wariacją bingo, gdzie zaimplementowany jest prosty system walki który używa zasad wspomnianej gry hazardowej. 

Zanim się zacznie rozgrywkę, warto najpierw przeczytać pomoc która jest w grze. Zwłaszcza, jeśli nie posiadamy żadnej wiedzy na temat bingo.

----------------------------------------------------------------------------------------------

The game is for mobile devices with Android OS, which is a variation of bingo with simple fight mechanics, which is using rules from previously mentioned gambling game.

Before you start to play, it is recommended for you to read in-game help. Especially, if you do not possess any knowledge about bingo.